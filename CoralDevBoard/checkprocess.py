# -*- coding: utf-8 -*-
import psutil
import os
import threading
import ConfigWiFi
import time
from subprocess import check_output

onlineAddr1 = 'www.cctv.cn'
onlineAddr2='www.google.com'

def router_connected():
    wifi_ip = check_output(['hostname', '-I'])
    result = False
    if wifi_ip != b'\n' and wifi_ip != b'192.168.101.2\n' and wifi_ip != b'192.168.100.2\n' and wifi_ip != b'192.168.45.1\n' and wifi_ip is not None:
        print(wifi_ip)
        result = True
    else:
        print("checkprocess no wifi connection")
        result = False
    return result

def is_online():
    if not router_connected():
        for x in range(8):
            time.sleep(5)
            if router_connected():
                break
    for x in range(3):
        response = os.system("ping -c 1 -w2 " + onlineAddr1 + " > /dev/null 2>&1")
        print("checkprocess ping result = ")
        print(response)
        # and then check the response...
        online = False
        if response == 0:
            online = True
            break
        else:
            response = os.system("ping -c 1 -w2 " + onlineAddr1 + " > /dev/null 2>&1")
            if response == 0:
                online = True
                break
            else:
                online = False
    if online:
        print('online')
    else:
        print('offline')

    return online



def is_running(script):

    if is_online():
        for q in psutil.process_iter():
            if q.name().startswith('python3'):
                if len(q.cmdline())>1 and script in q.cmdline()[1] and q.pid !=os.getpid():
                    #print("'{}' Process is already running".format(script))
                    return True
        os.system('python3 falldetect.pyc')
    return False

def configWifi():
    print("configWifi start, wait for 20 sec................")
    time.sleep(20)
    if not is_online():
        print('sudo python3 ConfigWiFi.pyc')
        os.system('sudo python3 ConfigWiFi.pyc')

def update():
    os.system('sudo python3 update.pyc')

def main():
    #time.sleep(10)
    # os.system("sudo pyaccesspoint stop")
    # os.system("nmcli radio all off")
    # os.system("nmcli radio all on")
    # os.system("sudo systemctl restart NetworkManager")

    thread = threading.Thread(target=configWifi, args=())
    thread.start()
    thread.join()

    time.sleep(10)
    if not is_online():
        os.system("sudo pyaccesspoint start")
        os.system("sudo pyaccesspoint stop")
        os.system("nmcli radio all off")
        os.system("nmcli radio all on")
        os.system("sudo systemctl restart NetworkManager")


    os.system('sudo python3 update.pyc')

    pTime = time.time()
    offlineTime = time.time()
    while True:

        text = "falldetect.pyc"
        threadRun = threading.Thread(target=is_running, args=(text,))
        threadRun.start()
        threadRun.join()

        time.sleep(5)
        cTime = time.time()
        if is_online():
            offlineTime = cTime
        print(cTime - offlineTime)
        if cTime - offlineTime > 300:
            os.system("sudo reboot")

        #每半小时检测一次是否有新版本
        # if cTime - pTime > 1800:
        #     threadUpdate = threading.Thread(target=update, args=())
        #     threadUpdate.start()
        #     threadUpdate.join()
        #     pTime = time.time()

if __name__ == "__main__":
    main()