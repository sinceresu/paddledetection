import os
import requests
import urllib.request
import time

###############################################################
#        Auto Update Program
# It checks the version from Web service with the version in
# version.dat. If the versions are different, download update.zip
#  and unzip it to update programs.
#
##############################################################

#################初始化变量#####################################
deviceSerial = 0
version = "0"
serialFile = 'config.dat'
versionFile = 'version.dat'
checkverAPI = "http://www.xiaomianao.net.cn:33333/api/update/fallcamera/"
serverAddr = 'ec2-3-26-5-20.ap-southeast-2.compute.amazonaws.com'
#################初始化变量#####################################


# read serial number and version from config.dat and version.dat
def read_config():
    global deviceSerial, version
    # read device serial
    if os.path.isfile(serialFile):
        # Using readlines()
        file1 = open(serialFile, 'r')
        Lines = file1.readlines()

        # Strips the newline character
        for line in Lines:
            if "serial" in line.lower():
                array = line.strip().split('=')
                if len(array) == 2:
                    try:
                        deviceSerial = int(array[1])
                    except:
                        print('config.dat cannot be read!')
        #print("devideSerial = ", deviceSerial)


        #read version
        if os.path.isfile(versionFile):
            # Using readlines()
            file1 = open(versionFile, 'r')
            Lines = file1.readlines()

            # Strips the newline character
            for line in Lines:
                if "version" in line.lower():

                    array = line.strip().split('=')
                    if len(array) == 2:
                        try:

                            version = array[1]

                        except:
                            print('version.dat cannot be read!')

        #print("version = ", version)

#compare the version from the web service and the version from local file
#if they are different, download update.zip and overwrite the program
def checkVersion():
    global deviceSerial, version, checkverAPI, serverAddr
    # response = requests.post('https://httpbin.org/post', data={'key': 'value'})
    # # Update an existing resource
    # requests.put('https://httpbin.org/put', data={'key': 'value'})
    # response.headers["date"]
    print("start update.................................")

    #check if the device is online
    response = os.system("ping -c 1 -w2 " + serverAddr + " > /dev/null 2>&1")

    online = False
    if response == 0:
        online = True
    else:
        online = False

    #if it's online, get version from the web service and comare withe version from the local file
    if online:
        try:
            deviceSerialbytes = deviceSerial.to_bytes(4, 'little')

            response = requests.get(checkverAPI + deviceSerialbytes.hex())
            #print("response.json()=", response.json())
            newVersion = response.json()["ver"]
            url = response.json()["url"]
            print(response.json()["url"])
            print("version", version)
            if (response.status_code == 200):
                if newVersion != version:
                    print("updating")
                    url_request = urllib.request.Request(url)
                    url_connect = urllib.request.urlopen(url_request)

                    # remember to open file in bytes mode
                    #down update.zip
                    with open("update.zip", 'wb') as f:
                        while True:
                            buffer_size = 100
                            buffer = url_connect.read(buffer_size)
                            if not buffer: break

                            # an integer value of size of written data
                            data_wrote = f.write(buffer)

                    # you could probably use with-open-as manner
                    url_connect.close()

                    #Delete py files
                    if os.path.exists(".falldetect.py"):
                        os.system('sudo find . -name \'falldetect.py\' -delete')
                    if os.path.exists(".checkprocess.py"):
                        os.system('sudo find . -name \'checkprocess.py\' -delete')
                    if os.path.exists(".update.py"):
                        os.system('sudo find . -name \'update.py\' -delete')
                    if os.path.exists(".ConfigWiFi.py"):
                        os.system('sudo find . -name \'ConfigWiFi.py\' -delete')
                    if os.path.exists(".SendUDP.py"):
                        os.system('sudo find . -name \'SendUDP.py\' -delete')


                    #unzip update.zip
                    os.system("unzip -o update.zip")

                    os.system("sudo rm version.dat")
                    with open("version.dat", 'w') as f:
                        f.write("version=" + newVersion)
                    time.sleep(10)

                    #assign rx permission to *.sh files
                    os.system("chmod a+rx *.sh")

                    os.system("sudo reboot")
                    #os._exit(1)
                else:
                    print("no new update found!")
            elif response.status_code == 404:
                print("Result not found!")
        except:
            print("update error!")

read_config()
checkVersion()