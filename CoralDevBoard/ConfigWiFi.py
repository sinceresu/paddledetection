# -*- coding: utf-8 -*-

###################################################################
#        WiFi Config Program
# It creates a web server and show a list of available WiFis.
# User can pick up da WiFi and type in password to connect.
# This program will save the SSID and password to wificonfig.dat
#
###################################################################


import os
from PyAccessPoint import pyaccesspoint
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
import time
from periphery import GPIO

hostName = ""
serverPort = 80
ssid_list = []
interface_name = 'wlan0'

#change LED color
def changeLED(color):

    ledR = GPIO("/dev/gpiochip4", 12, "out")  # pin 22
    ledG = GPIO("/dev/gpiochip4", 10, "out")  # pin 18
    ledB = GPIO("/dev/gpiochip2", 9, "out")  # pin 16

    if color == "W":  #RED
        ledR.write(False)
        ledG.write(False)
        ledB.write(False)
        print("falling")

    if color == "R":  #RED
        ledR.write(False)
        ledG.write(True)
        ledB.write(True)
        print("falling")

    if color == "OFF":  #Off
        ledR.write(True)
        ledG.write(True)
        ledB.write(True)
        print("nodetection")

    if color == "B":  #B
        ledR.write(True)
        ledG.write(True)
        ledB.write(False)
        print("config")

    if color == "G":  #G
        ledR.write(True)
        ledG.write(False)
        ledB.write(True)
        print("normal")

    if color == "W":  #RED
        ledR.write(False)
        ledG.write(True)
        ledB.write(False)
        print("falling")

# WiFi class
# check if the device is online.
# If it's online, return
# If it's offline, read SSID and password from wificonfig.dat and connect to WiFi
# If still offline, create a hotspot and web server to receive configuration

class WifiSetup():
    def __init__(self, onlineAddr1='www.cctv.cn', onlineAddr2='www.google.com'):
        self.onlineAddr1 = onlineAddr1
        self.onlineAddr2 = onlineAddr2
        self.ssid = ''
        self.password = ''
        self.configFile = 'wificonfig.dat'

    def checkOnline(self):

        #1. Turn LED off
        changeLED("OFF")
        print("ConfigWiFi.py start ..........................")

        #2. ping server
        response = os.system("ping -c 1 -w2 " + self.onlineAddr1 + " > /dev/null 2>&1")
        print("configwifi ping result = ")
        print(response)
        # and then check the response...
        online = False
        if response == 0:
            online = True
        else:
            response = os.system("ping -c 1 -w2 " + self.onlineAddr2 + " > /dev/null 2>&1")
            if response == 0:
                online = True
            else:
                online = False
        #online = False
        #if online, return. or run hotspot
        if online:
            return True
        else:
            # 3. 读取WiFi config，如果配置文件有信息，则连接WiFi
            self.readConfig()
            print("self.ssid=" + self.ssid)
            print("self.password=" + self.password)

            # 4. 如果从config文件中读取到了WiFi名和密码，则尝试连接，否则，建立热点
            if self.ssid != '' and self.password != '':
                wifiFinder = WifiFinder(server_name=self.ssid,
                           password=self.password,
                           interface='wlan0')
                #如果连接上了，则返回
                 
                if wifiFinder.run():
                    print("start to ping......")
                    response = os.system("ping -c 1 -w2 " + self.onlineAddr1 + " > /dev/null 2>&1")
                    print("ping -c 1 -w2 " + self.onlineAddr1)
                    if response == 0:
                        online = True
                    else:
                        response = os.system("ping -c 1 -w2 " + self.onlineAddr2 + " > /dev/null 2>&1")
                        print("ping -c 1 -w2 " + self.onlineAddr2)
                        if response == 0:
                            online = True
                        else:
                            online = False

                if online:
                    return
                else:
                    #5. if WiFi configuration doesn't work, create a hotspot
                    self.setupHotspot()
            else:
                #5. if there is no configuration, create a hotspot
                self.setupHotspot()

    # create a hotspot
    def setupHotspot(self):
        global ssid_list, interface_name

        #time.sleep(5)

        #1. restart wifi
        os.system("sudo pyaccesspoint stop")
        os.system("nmcli radio all off")
        os.system("nmcli radio all on")
        os.system("sudo systemctl restart NetworkManager")
        time.sleep(5)
        # os.system("sudo pyaccesspoint stop")
        # os.system("nmcli radio all off")
        # os.system("nmcli radio all on")
        # os.system("sudo systemctl restart NetworkManager")

        #2. get available WiFi list
        command = 'sudo iwlist {} scan | grep \'ESSID:\''
        result = os.popen(command.format(interface_name))
        result = list(result)

        for item in result:

            item_temp = item.strip().lstrip('ESSID:"').strip('"\n').strip()

            if len(ssid_list):
                ssid_list.append(item_temp)
            if item_temp not in ssid_list:
                ssid_list.append(item_temp)

        print("ssid_list", ssid_list)

        #3. create a hotspot
        hotspot = wifiHotspot()
        if hotspot.setupHotspot():

            #4. create a web server
            webServer = HTTPServer((hostName, serverPort), MyWebServer)

            print("Server started http://%s:%s" % (hostName, serverPort))

            try:
                thread = threading.Thread(target=webServer.serve_forever)
                thread.daemon = True
                thread.start()
                # webServer.serve_forever()
                print("Hotsport is ready!!!!!")
                changeLED("B")
            except KeyboardInterrupt:
                pass

            #5. check if the device is online, if it's online, that means wifi has been set up correctly.

            while True:

                time.sleep(5)
                response = os.system("ping -c 1 -w2 " + self.onlineAddr1 + " > /dev/null 2>&1")
                # and then check the response...
                response = 99999
                if response == 0:
                    print("cccccccccccccccccccccccccc")
                    changeLED("G")
                    webServer.server_close()
                    return True
                else:
                    response = os.system("ping -c 1 -w2 " + self.onlineAddr2 + " > /dev/null 2>&1")
                    response = 99999
                    if response == 0:
                        print("ddddddddddddddddddddd")
                        webServer.server_close()
                        return True

    # read config file and get ssid and password
    def readConfig(self):
        self.ssid = ''
        self.password = ''
        if os.path.isfile(self.configFile):
            with open(self.configFile, 'r') as f:
                lines = f.readlines()
                for line in lines:
                    if "ssid" in line:
                        self.ssid = line.split('=')[1]
                    if "password" in line:
                        self.password = line.split('=')[1]

# web server
class MyWebServer(BaseHTTPRequestHandler):

    # return available WiFi signals
    # and receive an SSID and password
    def do_GET(self):
        global ssid_list
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>", "utf-8"))
        self.wfile.write(bytes("<html><head><title>HealthTrack WIFI</title></head>", "utf-8"))
        #self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
        self.wfile.write(bytes("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />", "utf-8"))
        self.wfile.write(bytes("<body width=\"200\" style=\"background-color: \'#CCD1EE)\';\">", "utf-8"))
        self.wfile.write(bytes("<br><br><p style=\"text-align:center\">HealthTrack WiFi 配置</p>", "utf-8"))
        self.wfile.write(bytes("&nbsp;<div width=\"200\" height=\"500\" style=\"border:1px solid\">", "utf-8"))
        self.wfile.write(bytes("<form> &nbsp;&nbsp;&nbsp;WiFi 列表: <br>", "utf-8"))

        interface_name = 'wlan0'
        self.wfile.write(bytes("&nbsp;&nbsp;&nbsp;<select name= \"ssid\" id=\"ssid\">", "utf-8"))

        for ssid in ssid_list:
            if ssid != '':
                print("wifiname: ", "<option value= \"" + ssid + "\">")
                self.wfile.write(bytes("<option value= \"" + ssid + "\">" + ssid + "</option>", "utf-8"))

        self.wfile.write(bytes("</select><br><br>", "utf-8"))
        self.wfile.write(bytes("&nbsp;&nbsp;&nbsp;WiFi 密码: <br>", "utf-8"))
        self.wfile.write(bytes("&nbsp;&nbsp;&nbsp;<input type = \'text\' name = \'password\' ><br>", "utf-8"))
        #self.wfile.write(bytes("<input type=\'submit\' value=\'Send Request\'>", "utf-8"))
        self.wfile.write(bytes("<p align=\"center\"><input type=\'submit\' value=\'发送\'></p>", "utf-8"))
        self.wfile.write(bytes("</form>", "utf-8"))
        if '&' in self.path:
            # ssid=2222&password=33333
            self.ssid = self.path.split('&')[0].split('=')[1]
            self.password = self.path.split('&')[1].split('=')[1]
            self.wfile.write(bytes("<p>提交成功. WiFi名: %s, " % self.ssid, "utf-8"))
            self.wfile.write(bytes("<p>密码: %s</p>" % self.password, "utf-8"))

            # Server_name is a case insensitive string, and/or regex pattern which demonstrates
            # the name of targeted WIFI device or a unique part of it.
            # server_name = "example_name"
            # password = "your_password"
            changeLED("P")
            interface_name = "wlan0"  # i. e wlp2s0
            F = WifiFinder(server_name=self.ssid,
                           password=self.password,
                           interface=interface_name)
            if F.run():
                print("Finished")
                #os.system("python3 checkprocess.py")
                changeLED("G")
                os._exit(1)

                return
            else:
                changeLED("OFF")
                print("failed......................")
                os.system("sudo reboot")

        else:
            self.ssid = ''
            self.password = ''
            #self.wfile.write(bytes("<p>配置WiFi.</p>", "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))

# connect to a WiFi router
class WifiFinder:
    def __init__(self, *args, **kwargs):
        self.server_name = kwargs['server_name']
        self.password = kwargs['password']
        self.interface_name = kwargs['interface']
        self.main_dict = {}
        self.configFile = 'wificonfig.dat'

    def run(self):

        #restart wifi
        print("ConfigWiFi.py restart wifi service")
        # os.system("sudo pyaccesspoint stop")
        # os.system("nmcli radio all off")
        # os.system("nmcli radio all on")
        # os.system("sudo systemctl restart NetworkManager")
        # time.sleep(5)

        # os.system("sudo pyaccesspoint stop")
        print("ConfigWiFi.py restart wifi service 1")
        os.system("nmcli radio all off")
        print("ConfigWiFi.py restart wifi service 2")
        os.system("nmcli radio all on")
        print("ConfigWiFi.py restart wifi service 3")
        os.system("sudo systemctl restart NetworkManager")
        time.sleep(4)
        # connect to a WiFi router
        # command = """sudo iwlist {} scan | grep -ioE 'ssid:"(.*{}.*)'"""
        print("ConfigWiFi.py list wifi signals")
        command = 'sudo iwlist {} scan | grep -ioE \'ESSID:\"{}\"\''

        result = os.popen(command.format(self.interface_name, self.server_name))

        #time.sleep(5)
        # result = os.popen(command.format(self.interface_name, self.server_name))
        print("ConfigWiFi.py " + command.format(self.interface_name, self.server_name))
        result = list(result)
        print("ConfigWiFi.py result = ", result)
        if "Device or resource busy" in result or len(result) == 0:
            print("wifi name = ", result)
            return False
        else:
            ssid_list = [item.lstrip('ESSID:"').strip('"\n') for item in result]
            print("Successfully get ssids {}".format(str(ssid_list)))
        result = False
        #print("ssid_list[2] = ", ssid_list[2])

        #for name in ssid_list:
        for i in range(len(ssid_list)):

            name = ssid_list[i]

            try:
                # if
                if self.server_name.strip() == name.strip():

                    self.writeConfig(self.server_name.strip(), self.password)
                    result = self.connection(name.strip())

            except Exception as exp:
                print("Couldn't connect to name : {}. {}".format(name.strip(), exp))
                return False

            if result:

                print("Successfully connected to {}".format(name.strip()))
                # 写入配置文件
                #self.writeConfig(self.server_name.strip(), self.password)
                return True

        return False

    def connection(self, name):
        try:
            # os.system("sudo pyaccesspoint start")
            # os.system("sudo pyaccesspoint stop")
            # os.system("nmcli radio all off")
            # os.system("nmcli radio all on")
            os.system("sudo systemctl restart NetworkManager")
            print("******************************************************111")
            time.sleep(8)
            #os.system("nmcli dev wifi connect {} password {} ifname {}".format(name, self.password, self.interface_name))

            command = "nmcli dev wifi connect {} password {} ifname {}"

            #result = os.popen(command.format(name, self.password, self.interface_name))
            result = os.system(command.format(name, self.password, self.interface_name))


            print("nmcli dev wifi connect {} password {} ifname {}".format(name, self.password, self.interface_name))
            time.sleep(15)
            print("result = ", result)
            if "Error" in result:
                return False
            else:
                return True
            # os.system(
            #     "nmcli dev wifi connect {} password {} ifname {}".format(name, self.password, self.interface_name))
            print("1 nmcli dev wifi connect {} password {} ifname {}".format(name, self.password, self.interface_name))
            #time.sleep(10)
        except:
            print("failed to connect")
            raise
        else:
            return True



    def writeConfig(self, ssid, password):
        if ssid != '' and password != '':
                with open(self.configFile, 'w') as f:
                    f.write("ssid=" + ssid + "\r\npassword=" + password)

class wifiHotspot():
    def __init__(self):
        self.ssid = ''
        self.password = ''


    def setupHotspot(self):
        # Class parameters:
        # wlan - wlan interface
        # inet - wlan forward to interface (use None to off forwarding)
        # ip - just ip of your accesspoint wlan interface
        # netmask - so… -> (wiki link)
        # ssid - name of your accesspoint
        #password - password of your accesspoint
        access_point = pyaccesspoint.AccessPoint(wlan='wlan0', ssid='healthtrack', password='88888888')
        print("ssid = healthtrack, password = 88888888")
        #access_point = pyaccesspoint.AccessPoint()
        # access_point.wlan = "wlan0"
        # access_point.ssid = "healthtrack"
        # access_point.password = "88888888"
        access_point.start()
        time.sleep(5)
        if access_point.is_running():
            print('running1')
            return True
        else:
            print('not running')
            return False
        return False



def main():
    # print("start")
    # webServer = HTTPServer((hostName, serverPort), MyWebServer)
    # print("Server started http://%s:%s" % (hostName, serverPort))
    #
    # try:
    #     webServer.serve_forever()
    # except KeyboardInterrupt:
    #     pass
    #
    # webServer.server_close()
    # print("Server stopped.")
    mywifisetup = WifiSetup()
    mywifisetup.checkOnline()


if __name__ == "__main__":
    main()



#access_point.stop()
#access_point.is_running()