import socket
import threading

class SendUDP():
    def __init__(self, addr, port):
        self.addr = addr
        self.port = port

    def rs232_checksum(the_bytes):
        return b'%02X' % (sum(the_bytes) & 0xFF)

    def sendto(self, byteArray):
        print(self.addr)
        print(self.port)
        sock = socket.socket(socket.AF_INET,  # Internet
                             socket.SOCK_DGRAM)  # UDP
        sock.sendto(byteArray, (self.addr, self.port))

    def sendmessage(self, byteArray):
        try:
            th = threading.Thread(target=self.sendto, args=(byteArray,))
            th.start()
        except:
            print("TTS thread exception occurred")
            #self.writeLog('playsound', 'TTS thread exception occurred')



