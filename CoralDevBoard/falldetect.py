import mediapipe as mp
import cv2
import time
import math
from playsound import playsound
import pyttsx3
import threading
import argparse
import os.path

##################全局变量#####################
global globalPose      #保存当前和上一帧的pose数据
fallSpeed = 17          #跌倒速度
disappearFallSpeed = 16          #跌倒速度
videoWidth = 640
# videoHeight = 480
videoHeight = 360
floorLevelRatio = 0.5  # 地面在屏幕上的比例
##################全局变量#####################


class currentPoseClass:
    def __init__(self):

        self.head_x, self.head_y, self.head_score = 0, 0, 0

        self.nose_x, self.nose_y, self.nose_score = 0, 0, 0

        self.leftEyeInner_x, self.leftEyeInner_y, self.leftEyeInner_score = 0, 0, 0
        self.leftEye_x, self.leftEye_y, self.leftEye_score = 0, 0, 0
        self.leftEyeOuter_x, self.leftEyeOuter_y, self.leftEyeOuter_score = 0, 0, 0

        self.rightEyeInner_x, self.rightEyeInner_y, self.rightEyeInner_score = 0, 0, 0
        self.rightEye_x, self.rightEye_y, self.rightEye_score = 0, 0, 0
        self.rightEyeOuter_x, self.rightEyeOuter_y, self.rightEyeOuter_score = 0, 0, 0

        self.leftEar_x, self.leftEar_y, self.leftEar_score = 0, 0, 0
        self.rightEar_x, self.rightEar_y, self.rightEar_score = 0, 0, 0

        self.leftMouth_x, self.leftMouth_y, self.leftMouth_score = 0, 0, 0
        self.rightMouth_x, self.rightMouth_y, self.rightMouth_score = 0, 0, 0

        self.leftShoulder_x, self.leftShoulder_y, self.leftShoulder_score = 0, 0, 0
        self.rightShoulder_x, self.rightShoulder_y, self.rightShoulder_score = 0, 0, 0

        self.leftElbow_x, self.leftElbow_y, self.leftElbow_score = 0, 0, 0
        self.rightElbow_x, self.rightElbow_y, self.rightElbow_score = 0, 0, 0

        self.leftWrist_x, self.leftWrist_y, self.leftWrist_score = 0, 0, 0
        self.rightWrist_x, self.rightWrist_y, self.rightWrist_score = 0, 0, 0

        self.leftPinky_x, self.leftPinky_y, self.leftPinky_score = 0, 0, 0
        self.rightPinky_x, self.rightPinky_y, self.rightPinky_score = 0, 0, 0

        self.leftIndex_x, self.leftIndex_y, self.leftIndex_score = 0, 0, 0
        self.rightIndex_x, self.rightIndex_y, self.rightIndex_score = 0, 0, 0

        self.leftThumb_x, self.leftThumb_y, self.leftThumb_score = 0, 0, 0
        self.rightThumbx_x, self.rightThumb_y, self.rightThumb_score = 0, 0, 0

        self.leftHip_x, self.leftHip_y, self.leftHipx_score = 0, 0, 0
        self.rightHip_x, self.rightHip_y, self.rightHip_score = 0, 0, 0

        self.leftKnee_x, self.leftKnee_y, self.leftKnee_score = 0, 0, 0
        self.rightKnee_x, self.rightKnee_y, self.rightKnee_score = 0, 0, 0

        self.leftAnkle_x, self.leftAnkle_y, self.leftAnkle_score = 0, 0, 0
        self.rightAnkle_x, self.rightAnkle_y, self.rightAnkle_score = 0, 0, 0

        self.leftHeel_x, self.leftHeel_y, self.leftHeel_score = 0, 0, 0
        self.rightHeel_x, self.rightHeel_y, self.rightHeel_score = 0, 0, 0

        self.leftFootIndex_x, self.leftFootIndex_y, self.leftFootIndex_score = 0, 0, 0
        self.rightFootIndex_x, self.rightFootIndex_y, self.rightFootIndex_score = 0, 0, 0

        self.pre_head_x = 0
        self.pre_nose_y = 0
        self.pre_pre_head_x = 0
        self.pre_pre_head_y = 0

    def copyFromDetection(self, pose, width, height):
        #print(pose.landmark[0].x, pose.landmark[0].y)
        self.currentPose = pose

        self.nose_x, self.nose_y, self.nose_score = int(pose.landmark[0].x * width), int(pose.landmark[0].y * height), pose.landmark[0].visibility

        self.leftEyeInner_x, self.leftEyeInner_y, self.leftEyeInner_score = int(pose.landmark[1].x * width), int(pose.landmark[1].y * height), pose.landmark[1].visibility
        self.leftEye_x, self.leftEye_y, self.leftEye_score = int(pose.landmark[2].x * width), int(pose.landmark[2].y * height), pose.landmark[2].visibility
        self.leftEyeOuter_x, self.leftEyeOuter_y, self.leftEyeOuter_score = int(pose.landmark[3].x * width), int(pose.landmark[3].y * height), pose.landmark[3].visibility

        self.rightEyeInner_x, self.rightEyeInner_y, self.rightEyeInner_score = int(pose.landmark[4].x * width), int(pose.landmark[4].y * height), pose.landmark[4].visibility
        self.rightEye_x, self.rightEye_y, self.rightEye_score = int(pose.landmark[5].x * width), int(pose.landmark[5].y * height), pose.landmark[5].visibility
        self.rightEyeOuter_x, self.rightEyeOuter_y, self.rightEyeOuter_score = int(pose.landmark[6].x * width), int(pose.landmark[6].y * height), pose.landmark[6].visibility

        self.leftEar_x, self.leftEar_y, self.leftEar_score = int(pose.landmark[7].x * width), int(pose.landmark[7].y * height), pose.landmark[7].visibility
        self.rightEar_x, self.rightEar_y, self.rightEar_score = int(pose.landmark[8].x * width), int(pose.landmark[8].y * height), pose.landmark[8].visibility

        self.leftMouth_x, self.leftMouth_y, self.leftMouth_score = int(pose.landmark[9].x * width), int(pose.landmark[9].y * height), pose.landmark[9].visibility
        self.rightMouth_x, self.rightMouth_y, self.rightMouth_score = int(pose.landmark[10].x * width), int(pose.landmark[10].y * height), pose.landmark[10].visibility

        self.leftShoulder_x, self.leftShoulder_y, self.leftShoulder_score = int(pose.landmark[11].x * width), int(pose.landmark[11].y * height), pose.landmark[11].visibility
        self.rightShoulder_x, self.rightShoulder_y, self.rightShoulder_score = int(pose.landmark[12].x * width), int(pose.landmark[12].y * height), pose.landmark[12].visibility

        self.leftElbow_x, self.leftElbow_y, self.leftElbow_score = int(pose.landmark[13].x * width), int(pose.landmark[13].y * height), pose.landmark[13].visibility
        self.rightElbow_x, self.rightElbow_y, self.rightElbow_score = int(pose.landmark[14].x * width), int(pose.landmark[14].y * height), pose.landmark[14].visibility

        self.leftWrist_x, self.leftWrist_y, self.leftWrist_score = int(pose.landmark[15].x * width), int(pose.landmark[15].y * height), pose.landmark[15].visibility
        self.rightWrist_x, self.rightWrist_y, self.rightWrist_score = int(pose.landmark[16].x * width), int(pose.landmark[16].y * height), pose.landmark[16].visibility

        self.leftPinky_x, self.leftPinky_y, self.leftPinky_score = int(pose.landmark[17].x * width), int(pose.landmark[17].y * height), pose.landmark[17].visibility
        self.rightPinky_x, self.rightPinky_y, self.rightPinky_score = int(pose.landmark[18].x * width), int(pose.landmark[18].y * height), pose.landmark[18].visibility

        self.leftIndex_x, self.leftIndex_y, self.leftIndex_score = int(pose.landmark[19].x * width), int(pose.landmark[19].y * height), pose.landmark[19].visibility
        self.rightIndex_x, self.rightIndex_y, self.rightIndex_score = int(pose.landmark[20].x * width), int(pose.landmark[20].y * height), pose.landmark[20].visibility

        self.leftThumb_x, self.leftThumb_y, self.leftThumb_score = int(pose.landmark[21].x * width), int(pose.landmark[21].y * height), pose.landmark[21].visibility
        self.rightThumbx_x, self.rightThumb_y, self.rightThumb_score = int(pose.landmark[22].x * width), int(pose.landmark[22].y * height), pose.landmark[22].visibility

        self.leftHip_x, self.leftHip_y, self.leftHipx_score = int(pose.landmark[23].x * width), int(pose.landmark[23].y * height), pose.landmark[23].visibility
        self.rightHip_x, self.rightHip_y, self.rightHip_score = int(pose.landmark[24].x * width), int(pose.landmark[24].y * height), pose.landmark[24].visibility

        self.leftKnee_x, self.leftKnee_y, self.leftKnee_score = int(pose.landmark[25].x * width), int(pose.landmark[25].y * height), pose.landmark[25].visibility
        self.rightKnee_x, self.rightKnee_y, self.rightKnee_score = int(pose.landmark[26].x * width), int(pose.landmark[26].y * height), pose.landmark[0].visibility

        self.leftAnkle_x, self.leftAnkle_y, self.leftAnkle_score = int(pose.landmark[27].x * width), int(pose.landmark[27].y * height), pose.landmark[27].visibility
        self.rightAnkle_x, self.rightAnkle_y, self.rightAnkle_score = int(pose.landmark[28].x * width), int(pose.landmark[28].y * height), pose.landmark[28].visibility

        self.leftHeel_x, self.leftHeel_y, self.leftHeel_score = int(pose.landmark[29].x * width), int(pose.landmark[29].y * height), pose.landmark[29].visibility
        self.rightHeel_x, self.rightHeel_y, self.rightHeel_score = int(pose.landmark[30].x * width), int(pose.landmark[30].y * height), pose.landmark[30].visibility

        self.leftFootIndex_x, self.leftFootIndex_y, self.leftFootIndex_score = int(pose.landmark[31].x * width), int(pose.landmark[31].y * height), pose.landmark[31].visibility
        self.rightFootIndex_x, self.rightFootIndex_y, self.rightFootIndex_score = int(pose.landmark[32].x * width), int(pose.landmark[32].y * height), pose.landmark[32].visibility

        self.head_x = int((self.nose_x + self.leftEyeInner_x + self.leftEyeOuter_x + self.rightEyeInner_x + self.rightEyeOuter_x
            + self.leftEar_x + self.rightEar_x + self.leftMouth_x + self.rightMouth_x) / 9)
        self.head_y = int((self.nose_y + self.leftEyeInner_y + self.leftEyeOuter_y + self.rightEyeInner_y + self.rightEyeOuter_y
            + self.leftEar_y + self.rightEar_y + self.leftMouth_y + self.rightMouth_y) / 9)
        self.head_score = 1

    def copyFromCurrentPose(self):
        self.prePose = self.currentPose

        self.pre_pre_head_x = self.pre_head_x
        self.pre_pre_head_y = self.pre_nose_y

        self.pre_nose_x, self.pre_nose_y, self.pre_nose_score = self.nose_x, self.nose_y, self.nose_score

        self.pre_leftEyeInner_x, self.pre_leftEyeInner_y, self.pre_leftEyeInner_score = \
            self.leftEyeInner_x, self.leftEyeInner_y, self.leftEyeInner_score
        self.pre_leftEye_x, self.pre_leftEye_y, self.pre_leftEye_score = \
            self.leftEye_x, self.leftEye_y, self.leftEye_score
        self.pre_leftEyeOuter_x, self.pre_leftEyeOuter_y, self.pre_leftEyeOuter_score = \
            self.leftEyeOuter_x, self.leftEyeOuter_y, self.leftEyeOuter_score

        self.pre_rightEyeInner_x, self.pre_rightEyeInner_y, self.pre_rightEyeInner_score = \
            self.rightEyeInner_x, self.rightEyeInner_y, self.rightEyeInner_score
        self.pre_rightEye_x, self.pre_rightEye_y, self.pre_rightEye_score = \
            self.rightEye_x, self.rightEye_y, self.rightEye_score
        self.pre_rightEyeOuter_x, self.pre_rightEyeOuter_y, self.pre_rightEyeOuter_score = \
            self.rightEyeOuter_x, self.rightEyeOuter_y, self.rightEyeOuter_score

        self.pre_leftEar_x, self.pre_leftEar_y, self.pre_leftEar_score = self.leftEar_x, self.leftEar_y, self.leftEar_score
        self.pre_rightEar_x, self.pre_rightEar_y, self.pre_rightEar_score = self.rightEar_x, self.rightEar_y, self.rightEar_score

        self.pre_leftMouth_x, self.pre_leftMouth_y, self.pre_leftMouth_score = self.leftMouth_x, self.leftMouth_y, self.leftMouth_score
        self.pre_rightMouth_x, self.pre_rightMouth_y, self.pre_rightMouth_score = self.rightMouth_x, self.rightMouth_y, self.rightMouth_score

        self.pre_leftShoulder_x, self.pre_leftShoulder_y, self.pre_leftShoulder_score = self.leftShoulder_x, self.leftShoulder_y, self.leftShoulder_score
        self.pre_rightShoulder_x, self.pre_rightShoulder_y, self.pre_rightShoulder_score = self.rightShoulder_x, self.rightShoulder_y, self.rightShoulder_score

        self.pre_leftElbow_x, self.pre_leftElbow_y, self.pre_leftElbow_score = self.leftElbow_x, self.leftElbow_y, self.leftElbow_score
        self.pre_rightElbow_x, self.pre_rightElbow_y, self.pre_rightElbow_score = self.rightElbow_x, self.rightElbow_y, self.rightElbow_score

        self.pre_leftWrist_x, self.pre_leftWrist_y, self.pre_leftWrist_score = self.leftWrist_x, self.leftWrist_y, self.leftWrist_score
        self.pre_rightWrist_x, self.pre_rightWrist_y, self.pre_rightWrist_score = self.rightWrist_x, self.rightWrist_y, self.rightWrist_score

        self.pre_leftPinky_x, self.pre_leftPinky_y, self.pre_leftPinky_score = self.leftPinky_x, self.leftPinky_y, self.leftPinky_score
        self.pre_rightPinky_x, self.pre_rightPinky_y, self.pre_rightPinky_score = self.rightPinky_x, self.rightPinky_y, self.rightPinky_score

        self.pre_leftIndex_x, self.pre_leftIndex_y, self.pre_leftIndex_score = self.leftIndex_x, self.leftIndex_y, self.leftIndex_score
        self.pre_rightIndex_x, self.pre_rightIndex_y, self.pre_rightIndex_score = self.rightIndex_x, self.rightIndex_y, self.rightIndex_score

        self.pre_leftThumb_x, self.pre_leftThumb_y, self.pre_leftThumb_score = self.leftThumb_x, self.leftThumb_y, self.leftThumb_score
        self.pre_rightThumbx_x, self.pre_rightThumb_y, self.pre_rightThumb_score = self.rightThumbx_x, self.rightThumb_y, self.rightThumb_score

        self.pre_leftHip_x, self.pre_leftHip_y, self.pre_leftHipx_score = self.leftHip_x, self.leftHip_y, self.leftHipx_score
        self.pre_rightHip_x, self.pre_rightHip_y, self.pre_rightHip_score = self.rightHip_x, self.rightHip_y, self.rightHip_score

        self.pre_leftKnee_x, self.pre_leftKnee_y, self.pre_leftKnee_score = self.leftKnee_x, self.leftKnee_y, self.leftKnee_score
        self.pre_rightKnee_x, self.pre_rightKnee_y, self.pre_rightKnee_score = self.rightKnee_x, self.rightKnee_y, self.rightKnee_score

        self.pre_leftAnkle_x, self.pre_leftAnkle_y, self.pre_leftAnkle_score = self.leftAnkle_x, self.leftAnkle_y, self.leftAnkle_score
        self.pre_rightAnkle_x, self.pre_rightAnkle_y, self.pre_rightAnkle_score = self.rightAnkle_x, self.rightAnkle_y, self.rightAnkle_score

        self.pre_leftHeel_x, self.pre_leftHeel_y, self.pre_leftHeel_score = self.leftHeel_x, self.leftHeel_y, self.leftHeel_score
        self.pre_rightHeel_x, self.pre_rightHeel_y, self.pre_rightHeel_score = self.rightHeel_x, self.rightHeel_y, self.rightHeel_score

        self.pre_leftFootIndex_x, self.pre_leftFootIndex_y, self.pre_leftFootIndex_score = self.leftFootIndex_x, self.leftFootIndex_y, self.leftFootIndex_score
        self.pre_rightFootIndex_x, self.pre_rightFootIndex_y, self.pre_rightFootIndex_score = self.rightFootIndex_x, self.rightFootIndex_y, self.rightFootIndex_score

        self.pre_head_x = self.head_x
        self.pre_head_y = self.head_y
        self.head_score = 1
class detectClass:
    def __init__(self):
        self.moveDir = False  # 移动方向(角度)
        self.moveSpeed = 0  # 移动速度
        self.start_x, self.start_y = 0, 0  # 开始下降是的位置
        self.delayTimeAfterDisappear = 3  # 如果达到跌倒速度和方向，然后被遮挡后延续时间
        self.delayTimeAfterFall = 10  # 判断跌倒后，到报警时的延迟时间
        self.movingDown = False  # 是否向下移动
        self.fallen = False  # 是否跌倒
        self.falling = False  # 是否正在下跌
        self.disappearFalling = False  # 是否正在遮挡下跌
        self.fallStartTime = 0  # 跌倒开始时间
        self.disappearStartTime = 0  # 消失开始时间
        self.bodyLength = 0    #躯干长度
    def checkMovingDown(self, pose):
        if pose.head_y > pose.pre_head_y:
            return True
        return False

    def checkDirection(self, pose):
        if pose.pre_head_x - pose.head_x != 0:
            angle = (pose.head_y - pose.pre_head_y) / abs(pose.pre_head_x - pose.head_x)
        else:
            angle = 90
            self.moveDir = True
            return True
        if abs(angle) > 0.8:
            self.moveDir = True
            return True
        self.moveDir = False
        return False

    def checkSpeed(self, pose, fps):
        distance = math.sqrt((pose.pre_head_y - pose.head_y) ** 2 + (pose.pre_head_x - pose.head_x) ** 2)
        self.bodyLength = math.sqrt(((pose.pre_leftHip_x + pose.pre_rightHip_x) / 2 - pose.head_x) ** 2 \
                          + ((pose.pre_leftHip_y + pose.pre_rightHip_y) / 2 - pose.head_y) ** 2)
        self.faceLength = math.sqrt(((pose.pre_leftEye_x + pose.pre_rightEye_x) / 2 - (pose.pre_leftMouth_x + pose.pre_rightMouth_x) / 2) ** 2 \
                                    + ((pose.pre_leftEye_y + pose.pre_rightEye_y) / 2 - (pose.pre_leftMouth_y + pose.pre_rightMouth_y) / 2) ** 2)


        self.moveSpeed = distance / self.faceLength * fps
        #print("faceLength = ", self.faceLength, ", speed = ", self.moveSpeed)
        if(self.moveSpeed > fallSpeed):
            return True
        return False

    def checkDisappearSpeed(self, pose, fps):
        distance = math.sqrt((pose.pre_head_y - pose.head_y) ** 2 + (pose.pre_head_x - pose.head_x) ** 2)
        self.bodyLength = math.sqrt(((pose.pre_leftHip_x + pose.pre_rightHip_x) / 2 - pose.head_x) ** 2 \
                          + ((pose.pre_leftHip_y + pose.pre_rightHip_y) / 2 - pose.head_y) ** 2)
        self.faceLength = math.sqrt(((pose.pre_leftEye_x + pose.pre_rightEye_x) / 2 - (pose.pre_leftMouth_x + pose.pre_rightMouth_x) / 2) ** 2 \
                                    + ((pose.pre_leftEye_y + pose.pre_rightEye_y) / 2 - (pose.pre_leftMouth_y + pose.pre_rightMouth_y) / 2) ** 2)


        self.moveSpeed = distance / self.faceLength * fps
        #print("faceLength = ", self.faceLength, ", speed = ", self.moveSpeed)
        if(self.moveSpeed > disappearFallSpeed):
            return True
        return False

    def checkFalling(self, pose, fps, visable):
        #print("fallen = ", self.fallen)
        if self.falling:
            result = "falling"
        if self.fallen:
            result = "fallen"
        if (not self.falling) and (not self.fallen):
            result = "stand"
        if self.fallen is False:
            if visable:
                #print("***falling = ", self.fallen)
                #print(pose.head_y, " vs ", videoHeight * floorLevelRatio)
                # 如果处于站立状态

                if self.falling is False:
                    # 如果头部低于地平线, 认为正在跌到
                    # if pose.head_y > videoHeight * floorLevelRatio:
                    #     self.falling = True
                    #     self.fallStartTime = time.time()
                    #     #print("falling")
                    #     result = "falling"
                    #
                    # else:
                    # 检测是否下跌
                    if self.checkMovingDown(pose):
                        #print("Moving down")
                        # 检测是否是跌倒角度
                        if self.checkDirection(pose):
                            #print("Down direction")
                            # 检测是否达到下跌速度
                            if self.checkSpeed(pose, fps):
                                print("speed")
                                # 满足以上条件，认为是正在跌倒
                                self.start_x, start_y = pose.head_x, pose.head_y
                                self.falling = True
                                self.fallStartTime = time.time()
                                print("-----------")
                                # playsound("ding.mp3")
                                result = "falling"

                            if self.checkDisappearSpeed(pose, fps):
                                print("disappear speed")
                                # 满足以上条件，认为是正在跌倒
                                self.start_x, start_y = pose.head_x, pose.head_y
                                self.disappearFalling = True
                                self.disappearStartTime = time.time()


                    else:
                        if pose.head_y < videoHeight * 0.4:
                            self.falling = False
                            self.disappearFalling = False
                            self.fallen = False
                            self.moveDir = False
                            #print("ssssssssssssssssssssss")
                            result = "stand"
                else:    #如果处于跌倒状态, 超过特定时间，则认为是已经跌倒
                    # 如果处于正在跌倒状态, 头部高于地平线，则认为正在站起
                    if not self.checkMovingDown(pose) and pose.head_y < videoHeight * 0.4:
                        self.falling = False
                        self.disappearFalling = False
                        self.fallen = False
                        self.moveDir = False
                        print("@@@@@@@@@@正在站起")
                        result = "stand"
                    else:
                        #如果处于跌倒状态，并维持3秒钟，认为已经跌倒
                        cTime = time.time()
                        if cTime - self.fallStartTime > 5:
                            self.fallen = True
                            self.disappearFalling = False
                            self.falling = False
                            self.moveDir = False
                            print("$$$$$$跌倒了")
                            result = "fallen"
            else:
                if self.moveDir:
                    cTime = time.time()
                    if cTime - self.disappearStartTime > 5:
                        self.fallen = True
                        self.falling = False
                        self.disappearFalling = False
                        self.moveDir = False
                        print("跌倒了")
                        result = "fallen"

        else:   #如果已经跌到
            if visable:
                if pose.head_y < videoHeight * 0.5:
                    self.falling = False
                    self.disappearFalling = False
                    self.fallen = False
                    self.moveDir = False
                    #print("正在站起---")
                    result = "stand"
        #print("direction = ", self.moveDir)
        #print("result = " + result)
        return result

def playsound_thread(text):
    # os.system("./speech.sh " + "a    " + text)
    try:
        # 初始化
        engine = pyttsx3.init()
        voices = engine.getProperty('voices')
        engine.setProperty('voice', 'zh')
        voices = engine.getProperty('voices')
        # 語速控制
        rate = engine.getProperty('rate')
        # print(rate)
        engine.setProperty('rate', rate)

        # 音量控制
        volume = engine.getProperty('volume')
        # print(volume)
        # engine.setProperty('volume', volume * 10)

        engine.say(text)
        # 朗讀一次

        engine.runAndWait()
        engine.stop()
    except:
        self.writeLog('playsound_thread', 'TTS exception occurred')


    # engine.say('語音合成開始')
    # engine.say('我會說中文了，開森，開森')
    # engine.runAndWait()
    # engine.say('The quick brown fox jumped over the lazy dog.')
    # engine.runAndWait()
def textToSound(text):
    try:
        th = threading.Thread(target=playsound_thread, args=(text))
        th.start()
    except:
        #print("TTS thread exception occurred")
        self.writeLog('playsound', 'TTS thread exception occurred')
def main():
    parser = argparse.ArgumentParser(description='Fall detect')
    parser.add_argument('--videofile', type=str, default="N",
                        help='video file name')
    args = parser.parse_args()

    if os.path.exists(args.videofile):
            cap = cv2.VideoCapture(args.videofile)
    else:
        cap = cv2.VideoCapture(0)

    globalPose = currentPoseClass()
    fallDetect = detectClass()

    mp_pose = mp.solutions.pose
    mpDraw = mp.solutions.drawing_utils
    pose = mp_pose.Pose()
    fps = 0
    pTime = 0
    count = 0
    checkResult = "stand"
    fallcount = 0
    lastStatus = "stand"
    while True:
        success, frame = cap.read()
        if success == False:
            break
        frame = cv2.resize(frame, (videoWidth, videoHeight))
        rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        result = pose.process(rgb_frame)

        key = 0
        if result.pose_landmarks:
            globalPose.copyFromDetection(result.pose_landmarks, videoWidth, videoHeight)


            if count > 1:
                checkResult = fallDetect.checkFalling(globalPose, fps, True)

            else:
                count = 2
            globalPose.copyFromCurrentPose()
            mpDraw.draw_landmarks(frame, result.pose_landmarks, mp_pose.POSE_CONNECTIONS)
            #print(result.pose_landmarks.landmark)
            for id, lm in enumerate(result.pose_landmarks.landmark):
                x = int(lm.x * videoWidth)
                y = int(lm.y * videoHeight)
                cv2.circle(frame, (x, y), 2, (255, 0, 255), -1)
                #cv2.putText(frame, str(id), (x, y - 1), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
        else:
            if count > 1:
                checkResult = fallDetect.checkFalling(globalPose, fps, False)



        #print("result = ", checkResult)
        cTime = time.time()
        fps = int(1 / (cTime - pTime))
        if fps == 0:
            fps = 1
        pTime = cTime

        cv2.line(frame, (0, int(videoHeight * floorLevelRatio)), (videoWidth, int(videoHeight * floorLevelRatio)), (0, 0, 255), thickness=5)
        cv2.circle(frame, (globalPose.head_x, globalPose.head_y), 10, (0, 255, 0), -1)
        #print(globalPose.head_x, globalPose.head_y)
        fpsStr = str(fps)
        if result != "":
            displayText = "FPS:" + fpsStr + " " + checkResult
            if checkResult == "fallen":
                if lastStatus != "fallen":
                    fallcount = fallcount + 1
                    lastStatus = "fallen"

                #textToSound("有人跌倒了")
                cv2.putText(frame, displayText, (20, 70), cv2.FONT_HERSHEY_PLAIN,
                            5, (0, 0, 255), 3)
            else:
                lastStatus = "other"
                cv2.putText(frame, displayText, (20, 70), cv2.FONT_HERSHEY_PLAIN,
                            3, (0, 255, 0), 3)
        print("status = ", checkResult)
        cv2.putText(frame, str(fallcount), (40, 300), cv2.FONT_HERSHEY_PLAIN,
                    5, (0, 255, 255), 3)
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()