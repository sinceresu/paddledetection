# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/jde_predictor.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/jde_predictor.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/lapjv.cpp" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/lapjv.cpp.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/main.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/main.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/pipeline.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/pipeline.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/postprocess.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/postprocess.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/preprocess_op.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/preprocess_op.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/sde_predictor.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/sde_predictor.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/tracker.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/tracker.cc.o"
  "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/src/trajectory.cc" "/mnt/data/RefProjects/PaddleDetection/deploy/pptracking/cpp/CMakeFiles/main.dir/src/trajectory.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_MKL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "ext/yaml-cpp/src/ext-yaml-cpp/include"
  "/home/sujin/paddle_inference"
  "/home/sujin/paddle_inference/third_party/install/protobuf/include"
  "/home/sujin/paddle_inference/third_party/install/glog/include"
  "/home/sujin/paddle_inference/third_party/install/gflags/include"
  "/home/sujin/paddle_inference/third_party/install/xxhash/include"
  "/home/sujin/paddle_inference/third_party/boost"
  "/home/sujin/paddle_inference/third_party/eigen3"
  "/home/sujin/paddle_inference/paddle/include"
  "/home/sujin/paddle_inference/third_party/install/mklml/include"
  "/home/sujin/paddle_inference/third_party/install/mkldnn/include"
  "deps/opencv-3.4.16_gcc8.2_ffmpeg/include"
  "deps/opencv-3.4.16_gcc8.2_ffmpeg/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
