//   Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <ctime>
#include <memory>
#include <string>
#include <utility>
#include <vector>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "paddle_inference_api.h"  // NOLINT

#include "include/config_parser.h"
#include "include/keypoint_postprocess.h"
#include "include/preprocess_op.h"
#include "include/action_utils.h"

using namespace paddle_infer;

namespace PaddleDetection {


struct ActionResult {
  // Rectangle coordinates of detected object: left, right, top, down
  int mot_id;
  // action id of action object
  int action_id;
  // score of action
  float score;
  MOT_Rect rect;

};



class ActionVisualHelper {
  typedef struct ActionHistory_s {
    int life_remain;
    int action_id;
    int confirmed;
  }ActionHistory;

public:
  ActionVisualHelper(int frame_life=20) {
    frame_life_ = frame_life;
  }

  std::set<int> get_visualize_ids() {
      return  check_detected();
  }

  std::set<int>  check_detected(){
    auto id_detected = std::set<int>();
    std::vector<int>deperate_id;
    for (auto& action : action_history_) {
      action.second.life_remain -= 1;
      if (action.second.action_id == 0
      && action.second.confirmed
      ) {
        id_detected.insert(action.first);
      }
      if (action.second.life_remain == 0) {
        deperate_id.push_back(action.first);
      }
    }

    for (auto mot_id : deperate_id) {
      action_history_.erase(mot_id);
    }
    return id_detected;
  }


  void update(std::vector<ActionResult> action_res_list){
    for(const auto & action_result :  action_res_list) {
      auto mot_id = action_result.mot_id;
      auto action_res  = action_result.action_id;
      bool confirmed = false;
      if (action_history_.count(mot_id)) {
        if(action_history_[mot_id].action_id == 0 ) {
          if (action_res == 0 && judgeFallByRect(action_result.rect)) {
            confirmed = true;
          } else {
            if (judgeFallByRect(action_result.rect)) {
              action_history_[mot_id].life_remain = frame_life_;
              action_history_[mot_id].confirmed = true;
            }      
            continue;
          }
        }
      }

      ActionHistory  action_info = action_history_.count(mot_id) ? action_history_[mot_id] : ActionHistory();
      action_info.action_id = action_res;
      action_info.life_remain = frame_life_;
      action_info.confirmed = confirmed;

      action_history_[mot_id] = action_info;
    }
  }


private:
  bool judgeFallByRect(const MOT_Rect& person) {

    return (person.bottom - person.top)  / (person.right - person.left) < 1.4f;

  }
  int frame_life_;
  std::map<int , ActionHistory> action_history_;

};

// Visualiztion KeyPoint Result
cv::Mat VisualizeActionResult(const cv::Mat& img,
                             ActionVisualHelper & action_visual_helper,
                             const MOT_Result& mot_results, bool & action_triggered);
class ActionRecognizer {
 public:
  explicit ActionRecognizer(const std::string& model_dir,
                 const std::string& device="CPU",
                 bool use_mkldnn=false,
                 int cpu_threads=1,
                 const std::string& run_mode="paddle",
                 const int batch_size=1,
                 const int gpu_id = 0,
                 const int trt_min_shape=1,
                 const int trt_max_shape=1280,
                 const int trt_opt_shape=640,
                 bool trt_calib_mode=false,
                 int window_size=100,
                 bool random_pad=false) : 
                 kpt_buff_(window_size)
                 {
    this->device_ = device;
    this->gpu_id_ = gpu_id;
    this->cpu_math_library_num_threads_ = cpu_threads;
    this->use_mkldnn_ = use_mkldnn;

    this->trt_min_shape_ = trt_min_shape;
    this->trt_max_shape_ = trt_max_shape;
    this->trt_opt_shape_ = trt_opt_shape;
    this->trt_calib_mode_ = trt_calib_mode;
    config_.load_config(model_dir);
    this->use_dynamic_shape_ = config_.use_dynamic_shape_;
    this->min_subgraph_size_ = config_.min_subgraph_size_;
    this->window_size_ = window_size;
    // preprocessor_.Init(config_.preprocess_info_);
    LoadModel(model_dir, batch_size, run_mode);
  }

  // Load Paddle inference model
  void LoadModel(const std::string& model_dir,
                 const int batch_size = 1,
                 const std::string& run_mode = "paddle");

  // Run predictor
  void Predict(const MOT_Result & mot_results,
               const std::vector<KeyPointResult>& kpt_results,
               std::vector<ActionResult> &  act_results,
               const cv::Size& coord_size,
               std::vector<double>* times = nullptr
               );

  // Get Model Label list
  const std::vector<std::string>& GetLabelList() const {
    return config_.label_list_;
  }

 private:
  std::string device_ = "CPU";
  int gpu_id_ = 0;
  int cpu_math_library_num_threads_ = 1;
  bool use_dark = false;
  bool use_mkldnn_ = false;
  int min_subgraph_size_ = 3;
  bool use_dynamic_shape_ = false;
  int trt_min_shape_ = 1;
  int trt_max_shape_ = 1280;
  int trt_opt_shape_ = 640;
  bool trt_calib_mode_ = false;

  // Preprocess image and copy data to input buffer
  std::vector<float> Preprocess(const cv::Mat& input);
  // Postprocess result
  void Postprocess(ActionResult & act_result);

  std::shared_ptr<Predictor> predictor_;
  Preprocessor preprocessor_;
  ImageBlob inputs_;
  std::vector<float> output_data_;
  std::vector<int64_t> idx_data_;
  int window_size_;  

  ConfigPaser config_;
  KeyPointBuff kpt_buff_;  
};

}  // namespace PaddleDetection
