#pragma once

#include <ctime>
#include <memory>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <opencv2/core/core.hpp>

#include "include/jde_detector.h"
#include "include/keypoint_postprocess.h"



namespace PaddleDetection {
  
enum RawJointOrder{
  NOSE = 0,

  LEFT_EYE,
  RIGHT_EYE,
 
  LEFT_EAR,
  RIGHT_EAR, 

  LEFT_SHOULDER,
  RIGHT_SHOULDER,

  LEFT_ELBOW,
  RIGHT_ELBOW,

  LEFT_WRIST,
  RIGHT_WRIST,

  LEFT_HIP,
  RIGHT_HIP,

  LEFT_KNEE,
  RIGHT_KNEE,

  LEFT_ANKLE,
  RIGHT_ANKLE,

};

class KeyPointSequence {
public:
  KeyPointSequence(size_t max_size = 100){
      frames_ = 0;
      max_size_ = max_size;
  }

  bool save(const KeyPointResult & kpt, MOT_Track bbox) {
    kpts_.push_back(kpt);
    bboxes_.push_back(bbox);
    frames_ += 1;
    if (frames_ == max_size_) {
      return true;
    }
    return false;
  }


public:
  size_t frames_;
  std::vector<KeyPointResult> kpts_;
  std::vector<MOT_Track> bboxes_;
  size_t max_size_;

};



class KeyPointBuff final{

public:
  KeyPointBuff(size_t max_size) : max_size_(max_size){

  };
  ~KeyPointBuff() {

  };

  bool get_state(){
    return flag_to_pop_;

  }

  void update(const std::vector<KeyPointResult>& kpt_res, const MOT_Result& mot_res);
  std::vector<std::pair<int, KeyPointSequence>> get_collected_keypoint();

private:
  bool flag_track_interrupt_ = false;
  std::map<int, KeyPointSequence>keypoint_saver_;
  size_t max_size_;

  std::set<int> id_to_pop_;
  bool flag_to_pop_ = false;
        
};


}