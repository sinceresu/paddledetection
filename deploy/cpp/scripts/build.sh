
MACHINE_TYPE=`uname -m`
echo "MACHINE_TYPE: "${MACHINE_TYPE}

# 是否使用GPU(即是否使用 CUDA)
WITH_GPU=ON

# 是否使用MKL or openblas，TX2需要设置为OFF
if [ "$MACHINE_TYPE" = "x86_64" ]
then
WITH_MKL=ON
elif [ "$MACHINE_TYPE" = "aarch64" ]
then
WITH_MKL=OFF
else
  echo "Please set WITH_MKL manually"
fi

# 是否集成 TensorRT(仅WITH_GPU=ON 有效)
WITH_TENSORRT=ON

# paddle 预测库lib名称，由于不同平台不同版本预测库lib名称不同，请查看所下载的预测库中`paddle_inference/lib/`文件夹下`lib`的名称
PADDLE_LIB_NAME=libpaddle_inference

# TensorRT 的include路径
if [ "$MACHINE_TYPE" = "x86_64" ]
then
TENSORRT_INC_DIR=/home/sujin/TensorRT-8.4.0.6/include
elif [ "$MACHINE_TYPE" = "aarch64" ]
then
TENSORRT_INC_DIR=/usr/include/aarch64-linux-gnu
else
  echo "Please set TENSORRT_INC_DIR manually"
fi

# TensorRT 的lib路径
if [ "$MACHINE_TYPE" = "x86_64" ]
then
TENSORRT_LIB_DIR=/home/sujin/TensorRT-8.4.0.6/lib
elif [ "$MACHINE_TYPE" = "aarch64" ]
then
TENSORRT_LIB_DIR=/usr/lib/aarch64-linux-gnu
else
  echo "Please set TENSORRT_LIB_DIR manually"
fi

# Paddle 预测库路径
PADDLE_DIR=/home/sujin/paddle_inference

# CUDA 的 lib 路径
CUDA_LIB=/usr/local/cuda/lib64

# CUDNN 的 lib 路径
if [ "$MACHINE_TYPE" = "x86_64" ]
then
CUDNN_LIB=/usr/lib/x86_64-linux-gnu/
elif [ "$MACHINE_TYPE" = "aarch64" ]
then
CUDNN_LIB=/usr/lib/aarch64-linux-gnu
else
  echo "Please set CUDNN_LIB manually"
fi

# 是否开启关键点模型预测功能
WITH_KEYPOINT=ON

# 是否开启跟踪模型预测功能
WITH_MOT=ON

# 是否开启动作识别预测功能
WITH_ACTION=ON

WITH_CSI_CAMERA=ON



# if [ "$MACHINE_TYPE" = "x86_64" ]
# then
#   echo "set OPENCV_DIR for x86_64"
#   # linux系统通过以下命令下载预编译的opencv
#   mkdir -p $(pwd)/deps && cd $(pwd)/deps
#   wget -c https://paddledet.bj.bcebos.com/data/opencv-3.4.16_gcc8.2_ffmpeg.tar.gz
#   tar -xvf opencv-3.4.16_gcc8.2_ffmpeg.tar.gz && cd ..

#   # set OPENCV_DIR
#   OPENCV_DIR=$(pwd)/deps/opencv-3.4.16_gcc8.2_ffmpeg

# elif [ "$MACHINE_TYPE" = "aarch64" ]
# then
#   echo "set OPENCV_DIR for aarch64"
#   # TX2平台通过以下命令下载预编译的opencv
#   mkdir -p $(pwd)/deps && cd $(pwd)/deps
#   wget -c https://bj.bcebos.com/v1/paddledet/data/TX2_JetPack4.3_opencv_3.4.6_gcc7.5.0.tar.gz
#   tar -xvf TX2_JetPack4.3_opencv_3.4.6_gcc7.5.0.tar.gz && cd ..

#   # set OPENCV_DIR
#   OPENCV_DIR=$(pwd)/deps/TX2_JetPack4.3_opencv_3.4.6_gcc7.5.0/

# else
#   echo "Please set OPENCV_DIR manually"
# fi

# echo "OPENCV_DIR: "$OPENCV_DIR

# 以下无需改动
rm -rf build
mkdir -p build
cd build
cmake .. \
    -DWITH_GPU=${WITH_GPU} \
    -DWITH_MKL=${WITH_MKL} \
    -DWITH_TENSORRT=${WITH_TENSORRT} \
    -DTENSORRT_LIB_DIR=${TENSORRT_LIB_DIR} \
    -DTENSORRT_INC_DIR=${TENSORRT_INC_DIR} \
    -DPADDLE_DIR=${PADDLE_DIR} \
    -DCUDA_LIB=${CUDA_LIB} \
    -DCUDNN_LIB=${CUDNN_LIB} \
    -DOPENCV_DIR=${OPENCV_DIR} \
    -DPADDLE_LIB_NAME=${PADDLE_LIB_NAME} \
    -DWITH_KEYPOINT=${WITH_KEYPOINT} \
    -DWITH_MOT=${WITH_MOT} \
    -DCMAKE_BUILD_TYPE=Debug \
    -DWITH_CSI_CAMERA=${WITH_CSI_CAMERA}
make -j8
echo "make finished!"
