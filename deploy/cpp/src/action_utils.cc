#include <sstream>
// for setprecision
#include <chrono>
#include <iomanip>

#include <alsa/asoundlib.h>
#include <iostream>
#include <cstdint>
#include <unistd.h>
#include <fstream>

#include "include/action_utils.h"


namespace PaddleDetection {

namespace {
static std::vector<std::vector<int>> critical_joints= {{LEFT_SHOULDER, RIGHT_SHOULDER, NOSE}, {LEFT_KNEE, LEFT_ANKLE, RIGHT_KNEE, RIGHT_ANKLE}};
}

//pass when only one deteced in one group;

void KeyPointBuff::update(const std::vector<KeyPointResult>& kpt_res, const MOT_Result& mot_res) {

  std::set<int> updated_id;

  std::vector<bool> checked_index(kpt_res.size(), false);
  for (size_t idx = 0; idx < kpt_res.size(); idx++) {
    // checked_kpt_res.push_back(kpt_res[idx]);
    // checked_mot_res.push_back(mot_res[idx]);
    // continue;
    size_t joints = 0;
    for (; joints < critical_joints.size(); joints++) {
      bool found = false;

      for (size_t critical_joint = 0; critical_joint < critical_joints[joints].size(); critical_joint++) {
        const float* joint = &(kpt_res[idx].keypoints[3*critical_joints[joints][critical_joint]]);
        float conf= joint[0];
        if(conf > 0.25) {
          found = true;
          break;
        }

      }
      if (!found) {
        break;
      }

    }
    if (joints == critical_joints.size() ) 
    {
      checked_index[idx] = true;
    }
  }
  for (size_t idx = 0; idx < kpt_res.size(); idx++) {
    int tracker_id = mot_res[idx].ids;
    updated_id.insert(tracker_id);
    if (!checked_index[idx]) 
      continue;
    auto kpt_seq = keypoint_saver_.count(tracker_id) ? keypoint_saver_.at(tracker_id ) : KeyPointSequence(max_size_);
                                      
    bool is_full = kpt_seq.save(kpt_res[idx], mot_res[idx]);
    keypoint_saver_[tracker_id] = kpt_seq;

    //Scene1: result should be popped when frames meet max size
    if (is_full) {
      id_to_pop_.insert(tracker_id);
      flag_to_pop_ = true;
    }
  }
  //Scene2: result of a lost tracker should be popped
  std::vector<int> interrupted_id;
  for (const auto& keypoint : keypoint_saver_) {
    if (updated_id.count(keypoint.first) == 0) {
      interrupted_id.push_back(keypoint.first);
    }
  }
  if (interrupted_id.size() > 0) {
    flag_to_pop_ = true;
    id_to_pop_.insert(interrupted_id.begin(), interrupted_id.end());
  }

}

std::vector<std::pair<int, KeyPointSequence>> KeyPointBuff::get_collected_keypoint(){

  /*
        Output (List): List of keypoint results for Skeletonbased Recognition task, where 
                      the format of each element is [tracker_id, KeyPointSequence of tracker_id]
  */
  std::vector<std::pair<int, KeyPointSequence>> output;
  for (const auto& tracker_id : id_to_pop_) {
    const auto & old_kpq = keypoint_saver_[tracker_id];
    output.emplace_back(std::make_pair(tracker_id, old_kpq));
    if (old_kpq.frames_ == max_size_) {
      KeyPointSequence new_kpq = KeyPointSequence(max_size_);
      for (size_t i = 0; i < max_size_ / 2; i++) {
        new_kpq.save(old_kpq.kpts_[max_size_ / 2 + i], old_kpq.bboxes_[max_size_ / 2 + i]);
      }
      keypoint_saver_[tracker_id] = new_kpq;

    } else {
      keypoint_saver_.erase(tracker_id);
    }
  }

  flag_to_pop_ = false;
  id_to_pop_.clear();

  return output;
}


}

