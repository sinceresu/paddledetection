//   Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <sstream>
// for setprecision
#include <chrono>
#include <iomanip>
#include <algorithm>

#include "include/action_recognizer.h"

using namespace paddle_infer;

namespace PaddleDetection {
namespace {
static std::vector<std::vector<int>> critical_joints= {{LEFT_SHOULDER, RIGHT_SHOULDER, NOSE}, {LEFT_KNEE, LEFT_ANKLE, RIGHT_KNEE, RIGHT_ANKLE}};
}

namespace {
std::string GenerateCurrentDir() {
  char self[PATH_MAX] = { 0 };
  int nchar = readlink("/proc/self/exe", self, sizeof(self) );

  if (nchar < 0 || nchar >= sizeof(self))         
    return std::string();
  std::string path(self);

  std::string output(path.begin(), path.begin() + path.rfind("/"));
  return output;
}

// std::string alert_sound_file = GenerateCurrentDir() + "/" + "alert.wav";
//Time:Keys:Point

cv::Mat convert_keypoint_coordinary(std::vector<KeyPointResult> kpts, size_t win_size) {
  size_t joints = kpts[0].num_joints;
  cv::Mat keypoints(win_size, joints, CV_32FC2, cv::Scalar(0.f, 0.f));

  for (size_t i = 0; i < kpts.size(); i++) {
    for (int j = 0; j < kpts[i].num_joints; j++) {
      if (kpts[i].keypoints[j * 3] > 0.2f) {
        float x_coord = kpts[i].keypoints[j * 3 + 1];
        float y_coord = kpts[i].keypoints[j * 3 + 2];
        
        keypoints.at<cv::Point2f>(i, j)  = cv::Point2f(x_coord ,  y_coord);

      }

    }
  }
  return keypoints;

}
cv::Mat refine_keypoint_coordinary(std::vector<KeyPointResult> kpts,  std::vector<MOT_Track> bbox, cv::Size coord_size, size_t win_size) {
  if (kpts.empty()) {
    return cv::Mat();
  }
  size_t joints = kpts[0].num_joints;

  cv::Mat keypoints(win_size, joints, CV_32FC2, cv::Scalar(0.f, 0.f));

  for (size_t i = 0; i < kpts.size(); i++) {
    cv::Point2f tl(bbox[i].rects.left, bbox[i].rects.top);
    cv::Size2f wh = cv::Point2f(bbox[i].rects.right, bbox[i].rects.bottom) - tl;
    for (int j = 0; j < kpts[i].num_joints; j++) {
      if (kpts[i].keypoints[j * 3] > 0.2f) {
        float x_coord = kpts[i].keypoints[j * 3 + 1];
        float y_coord = kpts[i].keypoints[j * 3 + 2];

        keypoints.at<cv::Point2f>(i, j)  = cv::Point2f((x_coord - tl.x) / wh.width * coord_size.width ,\
         (y_coord - tl.y) / wh.height * coord_size.height);

      }

    }
  }

  return keypoints;
}
    
std::pair<std::vector<int>, std::vector<cv::Mat>> parse_mot_keypoint(std::vector<std::pair<int, KeyPointSequence>> input, cv::Size coord_size, size_t win_size) {

  std::vector<int> ids;
  std::vector<cv::Mat> skeleton;
  for (auto kpt_result:input) {
      ids.push_back(kpt_result.first);

      auto kpt_seq = kpt_result.second;
      auto kpts = kpt_seq.kpts_;
      auto bbox = kpt_seq.bboxes_;

      // cv::Mat raw_frame = convert_keypoint_coordinary(kpts, win_size);
      // cv::FileStorage fs("/home/sujin/RefProjects/PaddleDetection/deploy/cpp/src/action_cpp.yml", cv::FileStorage::WRITE);
      // fs <<"action"<< raw_frame;
      // fs.release();

      skeleton.push_back(refine_keypoint_coordinary(kpts, bbox, coord_size, win_size));

  }

  return  std::make_pair(ids, skeleton);
}


}
cv::Mat VisualizeActionResult(const cv::Mat& img,
                             ActionVisualHelper & visual_helper,
                             const MOT_Result& mot_results, bool & action_triggered) {
  static bool play_sound = true;
  cv::Mat vis_img = img.clone();
  int im_h = img.rows;
  int im_w = img.cols;
  float text_scale = std::max(1, int(im_w / 1600.));
  float text_thickness = 2.;

  auto id_detected = visual_helper.get_visualize_ids();
  for (const auto & mot_result : mot_results) {
    if (id_detected.count(mot_result.ids) ) {
      cv::Point text_position =
      cv::Point(mot_result.rects.left + (mot_result.rects.right - mot_result.rects.left) * 0.75 , mot_result.rects.top - 10);
      cv::putText(vis_img,
                "Falling",
                text_position,
                cv::FONT_ITALIC,
                text_scale,
                cv::Scalar(0, 0, 255),
                text_thickness);
      printf("Falling at %d, %d\n", text_position.x, text_position.y);
    } 
  }
  action_triggered = !id_detected.empty();
  return vis_img;

}
// Load Model and create model predictor
void ActionRecognizer::LoadModel(const std::string& model_dir,
                                 const int batch_size,
                                 const std::string& run_mode) {
  paddle_infer::Config config;
  std::string prog_file = model_dir + OS_PATH_SEP + "model.pdmodel";
  std::string params_file = model_dir + OS_PATH_SEP + "model.pdiparams";
  config.SetModel(prog_file, params_file);
  if (this->device_ == "GPU") {
    config.EnableUseGpu(200, this->gpu_id_);
    config.SwitchIrOptim(true);
    // use tensorrt
    if (run_mode != "paddle") {
      auto precision = paddle_infer::Config::Precision::kFloat32;
      if (run_mode == "trt_fp32") {
        precision = paddle_infer::Config::Precision::kFloat32;
      } else if (run_mode == "trt_fp16") {
        precision = paddle_infer::Config::Precision::kHalf;
      } else if (run_mode == "trt_int8") {
        precision = paddle_infer::Config::Precision::kInt8;
      } else {
        printf(
            "run_mode should be 'paddle', 'trt_fp32', 'trt_fp16' or "
            "'trt_int8'");
      }
      // set tensorrt
      config.EnableTensorRtEngine(1 << 30,
                                  batch_size,
                                  this->min_subgraph_size_,
                                  precision,
                                  true,
                                  this->trt_calib_mode_);

      // set use dynamic shape
      if (this->use_dynamic_shape_) {
        // set DynamicShsape for image tensor
        const std::vector<int> min_input_shape = {
            1, 3, this->trt_min_shape_, this->trt_min_shape_};
        const std::vector<int> max_input_shape = {
            1, 3, this->trt_max_shape_, this->trt_max_shape_};
        const std::vector<int> opt_input_shape = {
            1, 3, this->trt_opt_shape_, this->trt_opt_shape_};
        const std::map<std::string, std::vector<int>> map_min_input_shape = {
            {"image", min_input_shape}};
        const std::map<std::string, std::vector<int>> map_max_input_shape = {
            {"image", max_input_shape}};
        const std::map<std::string, std::vector<int>> map_opt_input_shape = {
            {"image", opt_input_shape}};

        config.SetTRTDynamicShapeInfo(
            map_min_input_shape, map_max_input_shape, map_opt_input_shape);
        std::cout << "TensorRT dynamic shape enabled" << std::endl;
      }
    }

  } else if (this->device_ == "XPU") {
    config.EnableXpu(10 * 1024 * 1024);
  } else {
    config.DisableGpu();
    if (this->use_mkldnn_) {
      config.EnableMKLDNN();
      // cache 10 different shapes for mkldnn to avoid memory leak
      config.SetMkldnnCacheCapacity(10);
    }
    config.SetCpuMathLibraryNumThreads(this->cpu_math_library_num_threads_);
  }
  config.SwitchUseFeedFetchOps(false);
  config.SwitchIrOptim(true);
  config.DisableGlogInfo();
  // Memory optimization
  config.EnableMemoryOptim();
  predictor_ = std::move(CreatePredictor(config));
}


std::vector<float> ActionRecognizer::Preprocess(const cv::Mat& input) {
  std::vector<float> coord_x;
  std::vector<float> coord_y;

  for (size_t t = 0; t < input.rows; t++) {
    cv::Point2f * row = (cv::Point2f *)input.ptr(t);
    for (size_t k = 0; k < input.cols; k++) {
      coord_x.push_back(row[k].x);
      coord_y.push_back(row[k].y);
    }
  }
  coord_x.insert(coord_x.end(), coord_y.begin(), coord_y.end());

  return coord_x;
}

void ActionRecognizer::Postprocess(ActionResult & act_result) {
   int  action_id = std::distance(output_data_.begin(), std::max_element(output_data_.begin(), output_data_.end()));
   float score = output_data_[action_id];
   act_result.action_id = action_id;
   act_result.score = score;
}

bool judgeFalldownByKeypoints(const std::vector<KeyPointResult> & kpts) {

  float nose_y = kpts.at(kpts.size() - 1).keypoints[NOSE*3 + 2] ;

  float left_knee_y = kpts.at(kpts.size() - 1).keypoints[NOSE*3 + 2] ;
  float knee_l_y = kpts.at(kpts.size() - 1).keypoints[NOSE*3 + 1] ;
  float end_y = kpts.at(kpts.size() - 1).keypoints[NOSE*3 + 2] ;

  const KeyPointResult& keypoints = kpts.at(kpts.size() - 1);
  size_t joints = 0;


  std::vector<int> critical_keypoint_heights;

  for (; joints < critical_joints.size(); joints++) {
    int max_score_height; 
    float max_score = -1.f;
    
    for (size_t critical_joint = 0; critical_joint < critical_joints[joints].size(); critical_joint++) {
        const float* joint = &(keypoints.keypoints[3*critical_joints[joints][critical_joint]]);
        float score= joint[0];
        float height= joint[2];
        if (score > max_score) {
          max_score = score;
          max_score_height = height;
        }
    }
    critical_keypoint_heights.push_back(max_score_height);

  }
  return true;
  // if (critical_keypoint_heights [0] - critical_keypoint_heights[1] > (rect.bottom - rect.top)  )
}

//
bool judgeFalldownByKeypointSequence(const std::vector<KeyPointResult> & kpts) {

  float start_y = kpts.at(0).keypoints[NOSE*3 + 2] ;
  float end_y = kpts.at(kpts.size() - 1).keypoints[NOSE*3 + 2] ;

  return start_y < end_y;

}

void ActionRecognizer::Predict(const MOT_Result & mot_results,
                               const std::vector<KeyPointResult>& kpt_results,
                               std::vector<ActionResult> &  act_results,
                               const cv::Size& coord_size,
                               std::vector<double>* times) {
  auto preprocess_start = std::chrono::steady_clock::now();

  // in_data_batch

  kpt_buff_.update(kpt_results, mot_results);
  bool state = kpt_buff_.get_state();// whether frame num is enough or lost tracker
  if(!state) {
    return;
  }
                        
  auto collected_keypoint = kpt_buff_.get_collected_keypoint();  //reoragnize kpt output with ID
  auto skeleton_action_input = parse_mot_keypoint(
    collected_keypoint, coord_size, window_size_);
  auto mot_id = skeleton_action_input.first;
  auto skeleton_list = skeleton_action_input.second;
  for (size_t i = 0; i < skeleton_list.size(); i++) {
    const auto & skeleton_action_input = skeleton_list[i];

    // cv::FileStorage fs("./action_cpp.yml", cv::FileStorage::WRITE);
    // fs<<"action"<<skeleton_action_input;
    // fs.release();
    auto in_data_all = Preprocess(skeleton_action_input);

    // Prepare input tensor
    auto input_names = predictor_->GetInputNames();
    for (const auto& tensor_name : input_names) {
      auto in_tensor = predictor_->GetInputHandle(tensor_name);
      if (tensor_name == "data_batch_0") {
        auto in_shape = in_tensor->shape();
        // int rh = inputs_.in_net_shape_[0];
        // int rw = inputs_.in_net_shape_[1];
        in_tensor->Reshape({1, 2, window_size_, 17, 1});
        in_tensor->CopyFromCpu(in_data_all.data());
      }
    }

    auto preprocess_end = std::chrono::steady_clock::now();
    std::vector<int> output_shape;
    // Run predictor
    auto inference_start = std::chrono::steady_clock::now();
    predictor_->Run();
    auto inference_end = std::chrono::steady_clock::now();

    auto postprocess_start = std::chrono::steady_clock::now();
    // Get output tensor
    auto output_names = predictor_->GetOutputNames();
    auto out_tensor = predictor_->GetOutputHandle(output_names[0]);
    output_shape = out_tensor->shape();
    // Calculate output length
    int output_size = 1;
    for (int j = 0; j < output_shape.size(); ++j) {
      output_size *= output_shape[j];
    }
    output_data_.resize(output_size);
    out_tensor->CopyToCpu(output_data_.data());

    // Postprocessing result
    ActionResult act_result;
    Postprocess(act_result);
    MOT_Track last_mot_track = collected_keypoint[i].second.bboxes_[collected_keypoint[i].second.bboxes_.size() - 1];
    
    if (judgeFalldownByKeypointSequence(collected_keypoint[i].second.kpts_))
    {
      if (judgeFalldownByKeypoints(collected_keypoint[i].second.kpts_))
      {
        
        act_result.mot_id = mot_id[i];
        act_result.rect = last_mot_track.rects;
        act_results.push_back(act_result);  
      }

    } 

    auto postprocess_end = std::chrono::steady_clock::now();

    std::chrono::duration<float> preprocess_diff =
        preprocess_end - preprocess_start;
    times->push_back(double(preprocess_diff.count() * 1000));
    std::chrono::duration<float> inference_diff = inference_end - inference_start;
    times->push_back(double(inference_diff.count() / 1000));
    std::chrono::duration<float> postprocess_diff =
        postprocess_end - postprocess_start;
    times->push_back(double(postprocess_diff.count() * 1000));
  }

}

}  // namespace PaddleDetection
