//   Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <glog/logging.h>

#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <chrono>
#include <thread>


#include <stdarg.h>
#include <sys/stat.h>


#include "SDL_audio.h"

#include <gflags/gflags.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include "include/jde_detector.h"
#include "include/object_detector.h"
#include "include/keypoint_detector.h"
#include "include/action_recognizer.h"
#include "include/ring_buffer.h"

#include "linux-kbhit.h"
#include "streamer.h"


DEFINE_string(model_dir, "", "Path of inference model");
DEFINE_string(model_dir_keypoint,
              "",
              "Path of keypoint detector inference model");
DEFINE_string(model_dir_action,
              "",
              "Path of action recognizer inference model");
DEFINE_int32(batch_size, 1, "batch_size");
DEFINE_int32(batch_size_keypoint, 8, "batch_size of keypoint detector");
DEFINE_string(
    video_file,
    "",
    "Path of input video, `video_file` or `camera_id` has a highest priority.");
DEFINE_int32(camera_id, -1, "Device id of camera to predict");
DEFINE_bool(
    use_gpu,
    false,
    "Deprecated, please use `--device` to set the device you want to run.");
DEFINE_string(device,
              "CPU",
              "Choose the device you want to run, it can be: CPU/GPU/XPU, "
              "default is CPU.");

DEFINE_string(output_dir, "output", "Directory of output visualization files.");
DEFINE_string(run_mode,
              "paddle",
              "Mode of running(paddle/trt_fp32/trt_fp16/trt_int8)");
DEFINE_int32(gpu_id, 0, "Device id of GPU to execute");
DEFINE_bool(run_benchmark,
            false,
            "Whether to predict a image_file repeatedly for benchmark");
DEFINE_bool(use_mkldnn, false, "Whether use mkldnn with CPU");
DEFINE_int32(cpu_threads, 1, "Num of threads with CPU");
DEFINE_int32(trt_min_shape, 1, "Min shape of TRT DynamicShapeI");
DEFINE_int32(trt_max_shape, 1280, "Max shape of TRT DynamicShapeI");
DEFINE_int32(trt_opt_shape, 640, "Opt shape of TRT DynamicShapeI");
DEFINE_bool(trt_calib_mode,
            false,
            "If the model is produced by TRT offline quantitative calibration, "
            "trt_calib_mode need to set True");

void PrintBenchmarkLog(std::vector<double> det_time, int img_num) {
  LOG(INFO) << "----------------------- Config info -----------------------";
  LOG(INFO) << "runtime_device: " << FLAGS_device;
  LOG(INFO) << "ir_optim: "
            << "True";
  LOG(INFO) << "enable_memory_optim: "
            << "True";
  int has_trt = FLAGS_run_mode.find("trt");
  if (has_trt >= 0) {
    LOG(INFO) << "enable_tensorrt: "
              << "True";
    std::string precision = FLAGS_run_mode.substr(4, 8);
    LOG(INFO) << "precision: " << precision;
  } else {
    LOG(INFO) << "enable_tensorrt: "
              << "False";
    LOG(INFO) << "precision: "
              << "fp32";
  }
  LOG(INFO) << "enable_mkldnn: " << (FLAGS_use_mkldnn ? "True" : "False");
  LOG(INFO) << "cpu_math_library_num_threads: " << FLAGS_cpu_threads;
  LOG(INFO) << "----------------------- Data info -----------------------";
  LOG(INFO) << "batch_size: " << FLAGS_batch_size;
  LOG(INFO) << "input_shape: "
            << "dynamic shape";
  LOG(INFO) << "----------------------- Model info -----------------------";
  FLAGS_model_dir.erase(FLAGS_model_dir.find_last_not_of("/") + 1);
  LOG(INFO) << "model_name: "
            << FLAGS_model_dir.substr(FLAGS_model_dir.find_last_of('/') + 1);
  LOG(INFO) << "----------------------- Perf info ------------------------";
  LOG(INFO) << "Total number of predicted data: " << img_num
            << " and total time spent(ms): "
            << std::accumulate(det_time.begin(), det_time.end(), 0);
  LOG(INFO) << "preproce_time(ms): " << det_time[0] / img_num
            << ", inference_time(ms): " << det_time[1] / img_num
            << ", postprocess_time(ms): " << det_time[2] / img_num;
}

static std::string DirName(const std::string& filepath) {
  auto pos = filepath.rfind(OS_PATH_SEP);
  if (pos == std::string::npos) {
    return "";
  }
  return filepath.substr(0, pos);
}

static bool PathExists(const std::string& path) {
#ifdef _WIN32
  struct _stat buffer;
  return (_stat(path.c_str(), &buffer) == 0);
#else
  struct stat buffer;
  return (stat(path.c_str(), &buffer) == 0);
#endif  // !_WIN32
}

static void MkDir(const std::string& path) {
  if (PathExists(path)) return;
  int ret = 0;
#ifdef _WIN32
  ret = _mkdir(path.c_str());
#else
  ret = mkdir(path.c_str(), 0755);
#endif  // !_WIN32
  if (ret != 0) {
    std::string path_error(path);
    path_error += " mkdir failed!";
    throw std::runtime_error(path_error);
  }
}

static void MkDirs(const std::string& path) {
  if (path.empty()) return;
  if (PathExists(path)) return;

  MkDirs(DirName(path));
  MkDir(path);
}
#ifndef MAX_BUF
#define MAX_BUF 200
#endif
std::string GenerateTimeStr() {
  std::time_t rawtime;
  std::tm* timeinfo;
  char buffer [80];
  std::time(&rawtime);
  timeinfo = std::localtime(&rawtime);
  std::strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
  return std::string(buffer);

}

std::string GenerateCurrentDir() {
  char self[PATH_MAX] = { 0 };
  int nchar = readlink("/proc/self/exe", self, sizeof(self) );

  if (nchar < 0 || nchar >= sizeof(self))         
    return std::string();
  std::string path(self);

  std::string output(path.begin(), path.begin() + path.rfind("/"));
  return output;
}


#if defined  WITH_CSI_CAMERA
std::string gstreamer_pipeline (int capture_width, int capture_height, int display_width, int display_height, int framerate, int flip_method) {
    return "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(capture_width) + ", height=(int)" +
           std::to_string(capture_height) + ", framerate=(fraction)" + std::to_string(framerate) +
           "/1 ! nvvidconv flip-method=" + std::to_string(flip_method) + " ! video/x-raw, width=(int)" + std::to_string(display_width) + ", height=(int)" +
           std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! videobalance contrast=1.5 brightness=-0.2 saturation=2.0 ! appsink";
      //     std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}

#endif
void PredictVideo(const std::string& video_path,
                  PaddleDetection::JDEDetector* mot,
                  PaddleDetection::KeyPointDetector* keypoint,
                  PaddleDetection::ActionRecognizer* action,
                 
                  const std::string& output_dir = "output") {
  // Open video
  cv::VideoCapture capture;
  std::string video_out_name = "output.mp4";
  bool is_file = false;
  if (FLAGS_camera_id != -1) {
#if defined  WITH_CSI_CAMERA
    int capture_width = 640 ;
    int capture_height = 360 ;
    int display_width = 640 ;
    int display_height = 360 ;
    int framerate = 20 ;
    int flip_method = 0 ;

    std::string pipeline = gstreamer_pipeline(capture_width,
    capture_height,
    display_width,
    display_height,
    framerate,
    flip_method);
    capture.open(pipeline, cv::CAP_GSTREAMER);
#else
    capture.open(FLAGS_camera_id);
    // if(capture.isOpened())
    // {
    //   capture.set(cv::CAP_PROP_FRAME_WIDTH, 1280);
    //   capture.set(cv::CAP_PROP_FRAME_HEIGHT, 720);
    //   capture.set(cv::CAP_PROP_FPS, 30);
    // }
#endif
  } else {
    capture.open(video_path);
    is_file = video_path.find("rtsp") == std::string::npos ? true : false;
    video_out_name =
        video_path.substr(video_path.find_last_of(OS_PATH_SEP) + 1);
        if (video_out_name.find("mp4") == std::string::npos) {
          video_out_name += ".mp4";
        }
  }
  if (!capture.isOpened()) {
    printf("can not open video : %s\n", video_path.c_str());
    return;
  }

  auto skeleton_action_visual_helper = PaddleDetection::ActionVisualHelper(
                      110);
  // Get Video info : resolution, fps, frame count
  int video_width = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
  int video_height = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
  int video_fps = static_cast<int>(capture.get(cv::CAP_PROP_FPS));
  //int video_frame_count =
  //    static_cast<int>(capture.get(cv::CAP_PROP_FRAME_COUNT));
  //printf("fps: %d, frame_count: %d\n", video_fps, video_frame_count);
  printf("fps: %d,\n", video_fps);
  // Create VideoWriter for output
  cv::VideoWriter video_out;  
  std::string video_out_path(output_dir);
  if (output_dir.rfind(OS_PATH_SEP) != output_dir.size() - 1) {
    video_out_path += OS_PATH_SEP;
  }

  video_out_path += GenerateTimeStr() + "_" + video_out_name;

  int fcc = cv::VideoWriter::fourcc('X', '2', '6', '4');
  video_out.open(video_out_path.c_str(),
                 fcc,
                 video_fps,
                 cv::Size(video_width, video_height),
                 true);
  if (!video_out.isOpened()) {
    printf("create video writer failed!\n");
    return;
  }
  PaddleDetection::PoseSmooth smoother =
      PaddleDetection::PoseSmooth(video_width, video_height);
  // std::unique_ptr<Streamer> streamer = std::unique_ptr<Streamer>(new Streamer());
  // streamer->SetEncodeParam(120, 30);
  // std::stringstream streaming_url;
  // streaming_url << "rtsp://localhost:" << 8554 << "/live/test" ;
  // if (0 != streamer->Initialize(video_width, video_height, streaming_url.str(), video_fps, 0)) {
  //   streamer->Release();
  //   LOG(WARNING) << "Failed to initialize rtsp pusher!";
  //   return;
  // }
  std::vector<PaddleDetection::MOT_Result> result_mots;
  std::vector<double> det_times(3);
  std::vector<double> keypoint_times(3);


 
  // Store keypoint results
  std::vector<PaddleDetection::KeyPointResult> result_kpts;
  std::vector<cv::Mat> imgs_kpts;
  std::vector<std::vector<float>> center_bs;
  std::vector<std::vector<float>> scale_bs;
  std::vector<int> colormap_kpts = PaddleDetection::GenerateColorMap(20);

  double detect_time = 0;
  double keypoint_time = 0;
  double action_time = 0;

  // Capture all frames and do inference
  cv::Mat frame;
  int frame_id = 1;
  cv::namedWindow("Action Recognition Demo", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);//CV_WINDOW_NORMAL就是0
  cv::resizeWindow("Action Recognition Demo",cv::Size(640,360));
  cv::moveWindow("Action Recognition Demo",320,180);
  //cv::setWindowProperty("Action Recognition Demo", cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
 // circular_buffer<cv::Mat> circular_buffer(2);
  bool stop_flag = false;
  bool end_flag = false;
  SDL_AudioSpec wavSpec;
  uint32_t wavLength;
  uint8_t *wavBuffer;

  std::string alert_sound_file = GenerateCurrentDir() + "/" + "alert.wav";

  SDL_LoadWAV(alert_sound_file.c_str(), &wavSpec, &wavBuffer, &wavLength);
  SDL_AudioDeviceID deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);
  
  auto start_time = std::chrono::steady_clock::now();
  while (!end_flag) { 
    std::vector<cv::Mat> imgs;


    for (int bs_idx = 0; bs_idx < FLAGS_batch_size; bs_idx++) {
      //while (!end_flag) { 
        cv::Mat frame;
        capture.read(frame);

        if (frame.empty()) {
	 end_flag = true;	
         break;
        }
       // printf("frame width: %d, height: %d\n", frame.cols, frame.rows);
        imgs.push_back(std::move(frame));
       // std::this_thread::sleep_for(std::chrono::milliseconds(10));

    //  }

    }
    if (end_flag)
      break;

    result_mots.clear();
    mot->Predict(imgs, 0, 1, &result_mots, &det_times);

    detect_time = std::accumulate(det_times.begin(), det_times.end(), 0) / (frame_id + FLAGS_batch_size - 1);
    
    result_kpts.clear();
    int total_imsize = 0;

    for (int bs_idx = 0; bs_idx < FLAGS_batch_size; bs_idx++) {
      PaddleDetection::MOT_Result& result = result_mots[bs_idx];
      // if (!result.empty())
      //   printf("detect frame: %d, sore : %f\n", frame_id + bs_idx, result[0].score);

      int imsize = result.size();
      total_imsize += imsize;
    }

    int img_idx = 0;
    for (int bs_idx = 0; bs_idx < FLAGS_batch_size; bs_idx++) {
      PaddleDetection::MOT_Result& result = result_mots[bs_idx];
      int imsize = result.size();

      for (int i = 0; i < imsize; i++, img_idx++) {
        auto item = result[i];
        cv::Mat crop_img;
        std::vector<int> rect = {
            (int)round(item.rects.left), (int)round(item.rects.top), (int)round(item.rects.right), (int)round(item.rects.bottom)};
        std::vector<float> center;
        std::vector<float> scale;
        
        {
          PaddleDetection::CropImg(imgs[bs_idx], crop_img, rect, center, scale);
          center_bs.emplace_back(center);
          scale_bs.emplace_back(scale);
          imgs_kpts.emplace_back(crop_img);
        }

        if (imgs_kpts.size() == FLAGS_batch_size_keypoint ||
            ((img_idx == total_imsize - 1) && !imgs_kpts.empty())) {
          keypoint->Predict(imgs_kpts,
                            center_bs,
                            scale_bs,
                            0,
                            1,
                            &result_kpts,
                            &keypoint_times);

          imgs_kpts.clear();
          center_bs.clear();
          scale_bs.clear();
        }
      }
    }
    keypoint_time = std::accumulate(keypoint_times.begin(), keypoint_times.end(), 0) / (frame_id + FLAGS_batch_size - 1);
  
    for (int bs_idx = 0, kpts = 0; bs_idx < FLAGS_batch_size; bs_idx++) {
      PaddleDetection::MOT_Result& result = result_mots[bs_idx];

      std::vector<PaddleDetection::KeyPointResult> kpt_results(&result_kpts[kpts], &result_kpts[kpts + result.size()]);

      // std::copy_n(&result_kpts[kpts], result.size(), kpt_results.end());

      kpts += result.size();
      if (kpt_results.size() == 1) {
        for (int i = 0; i < kpt_results.size(); i++) {
          kpt_results[i] = smoother.smooth_process(&(kpt_results[i]));
        }
      }

      std::vector<PaddleDetection::ActionResult>   result_action;

      std::vector<double> action_times;
      action->Predict(    result,
                          kpt_results,
                          result_action, 
                          cv::Size(384, 512),
                          &action_times);
      skeleton_action_visual_helper.update(result_action);

      action_time += std::accumulate(action_times.begin(), action_times.end(), 0) ;
      
      cv::Mat kpt_im = VisualizeKptsResult(imgs[bs_idx], kpt_results, colormap_kpts);

      static int play_frames = 0;
      static bool playing_audio = false;
      bool action_triggered = false;
      cv::Mat action_im = VisualizeActionResult(kpt_im, skeleton_action_visual_helper, result, action_triggered);

      if (action_triggered) {
        if (!playing_audio) {
          int success = SDL_QueueAudio(deviceId, wavBuffer, wavLength);
	        SDL_PauseAudioDevice(deviceId, 0);     
          playing_audio = false;
          play_frames = true;
          
        }
      }
      if (playing_audio) {
        play_frames++;
        if (play_frames >= 100) 
          play_frames = 0;
          playing_audio = false;
      }


      cv::Mat result_im = PaddleDetection::VisualizeTrackResult(
          action_im, result, 1000. / (detect_time + keypoint_time + action_time / frame_id), frame_id);   
      //if (is_file)      
      video_out.write(result_im);
      // if (0 != streamer->SendImage(result_im, frame_id * 1000 / video_fps )) {
      //   LOG(WARNING) << "Failed to streaming!";
      // }
      cv::imshow("Action Recognition Demo", result_im);
      if (cv::waitKey(1) == char('q')) {
       printf("Key Q Pressed, Exitting!\n");
       end_flag = true;
       break;
      }    

      if (kbhit()) {
         printf("Key Q Pressed, Exitting!\n");
         end_flag = true;
         break;
      }

      if ((frame_id % 100) == 0) {
        auto end_time = std::chrono::steady_clock::now();

        const double wall_clock_seconds =
          std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time)
              .count();

        LOG(INFO) << "Process " << frame_id << " frames, frame rate: " << std::setprecision(4) << frame_id / wall_clock_seconds << "fps!";

        
      }
      frame_id++;
      
    }
 
  }


  stop_flag = true;
  //cap_thread.join();
  
	SDL_CloseAudioDevice(deviceId);
	SDL_FreeWAV(wavBuffer);
	// SDL_Quit();

  capture.release();
  video_out.release();
  // streamer->Release();

  printf("PrintBenchmarkLog: object deteciton\n");
  PrintBenchmarkLog(det_times, frame_id);

  printf("PrintBenchmarkLog: keypoint deteciton\n");
  PrintBenchmarkLog(keypoint_times, frame_id);

  printf("Visualized output saved as %s\n", video_out_path.c_str());
}

int main(int argc, char** argv) {
  // Parsing command-line
  google::ParseCommandLineFlags(&argc, &argv, true);
  if (FLAGS_model_dir.empty() || FLAGS_video_file.empty()) {
    std::cout << "Usage: ./main --model_dir=/PATH/TO/INFERENCE_MODEL/ "
              << "--video_file=/PATH/TO/INPUT/VIDEO/" << std::endl;
    return -1;
  }
  if (!(FLAGS_run_mode == "paddle" || FLAGS_run_mode == "trt_fp32" ||
        FLAGS_run_mode == "trt_fp16" || FLAGS_run_mode == "trt_int8")) {
    std::cout
        << "run_mode should be 'paddle', 'trt_fp32', 'trt_fp16' or 'trt_int8'.";
    return -1;
  }
  transform(FLAGS_device.begin(),
            FLAGS_device.end(),
            FLAGS_device.begin(),
            ::toupper);
  if (!(FLAGS_device == "CPU" || FLAGS_device == "GPU" ||
        FLAGS_device == "XPU")) {
    std::cout << "device should be 'CPU', 'GPU' or 'XPU'.";
    return -1;
  }
  if (FLAGS_use_gpu) {
    std::cout << "Deprecated, please use `--device` to set the device you want "
                 "to run.";
    return -1;
  }

  // Do inference on input video or image
  PaddleDetection::JDEDetector mot1(FLAGS_model_dir,
                                   FLAGS_device,
                                   FLAGS_use_mkldnn,
                                   FLAGS_cpu_threads,
                                   "paddle",
                                   FLAGS_batch_size,
                                   FLAGS_gpu_id,
                                   FLAGS_trt_min_shape,
                                   FLAGS_trt_max_shape,
                                   FLAGS_trt_opt_shape,
                                   FLAGS_trt_calib_mode);
  PaddleDetection::KeyPointDetector* keypoint1 = nullptr;
  if (!FLAGS_model_dir_keypoint.empty()) {
    keypoint1 = new PaddleDetection::KeyPointDetector(FLAGS_model_dir_keypoint,
                                                     FLAGS_device,
                                                     FLAGS_use_mkldnn,
                                                     FLAGS_cpu_threads,
                                                     FLAGS_run_mode,
                                                     FLAGS_batch_size_keypoint,
                                                     FLAGS_gpu_id,
                                                     FLAGS_trt_min_shape,
                                                     FLAGS_trt_max_shape,
                                                     FLAGS_trt_opt_shape,
                                                     FLAGS_trt_calib_mode,
                                                     false);
  }
  PaddleDetection::ActionRecognizer* action1 = nullptr;
  if (!FLAGS_model_dir_action.empty()) {
    action1 = new PaddleDetection::ActionRecognizer(FLAGS_model_dir_action,
                                                     FLAGS_device,
                                                     FLAGS_use_mkldnn,
                                                     FLAGS_cpu_threads,
                                                     "paddle",
                                                     FLAGS_batch_size_keypoint,
                                                     FLAGS_gpu_id,
                                                     FLAGS_trt_min_shape,
                                                     FLAGS_trt_max_shape,
                                                     FLAGS_trt_opt_shape,
                                                     FLAGS_trt_calib_mode,
                                                     50);
  }
   

  if (!PathExists(FLAGS_output_dir)) {
    MkDirs(FLAGS_output_dir);
  }

  PredictVideo(FLAGS_video_file, &mot1, keypoint1, action1, FLAGS_output_dir);

  delete keypoint1;
  keypoint1 = nullptr;  

  delete action1;
  action1 = nullptr;  

  return 0;
}
