//   Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <chrono>
#include <thread>


#include <stdarg.h>
#include <sys/stat.h>

#include <gflags/gflags.h>
#include <glog/logging.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>


#ifdef _WIN32
#define OS_PATH_SEP "\\"
#else
#define OS_PATH_SEP "/"
#endif

DEFINE_int32(camera_id, -1, "Device id of camera to predict");
DEFINE_int32(frame_rate, 30, "Camera capture frame rate");
DEFINE_string(output_dir, "output", "Directory of output visualization files.");
DEFINE_double(brightness, 0., "Brightness of camera.");


static std::string DirName(const std::string& filepath) {
  auto pos = filepath.rfind(OS_PATH_SEP);
  if (pos == std::string::npos) {
    return "";
  }
  return filepath.substr(0, pos);
}

static bool PathExists(const std::string& path) {
#ifdef _WIN32
  struct _stat buffer;
  return (_stat(path.c_str(), &buffer) == 0);
#else
  struct stat buffer;
  return (stat(path.c_str(), &buffer) == 0);
#endif  // !_WIN32
}

static void MkDir(const std::string& path) {
  if (PathExists(path)) return;
  int ret = 0;
#ifdef _WIN32
  ret = _mkdir(path.c_str());
#else
  ret = mkdir(path.c_str(), 0755);
#endif  // !_WIN32
  if (ret != 0) {
    std::string path_error(path);
    path_error += " mkdir failed!";
    throw std::runtime_error(path_error);
  }
}

static void MkDirs(const std::string& path) {
  if (path.empty()) return;
  if (PathExists(path)) return;

  MkDirs(DirName(path));
  MkDir(path);
}
#ifndef MAX_BUF
#define MAX_BUF 200
#endif
std::string GenerateTimeStr() {
  std::time_t rawtime;
  std::tm* timeinfo;
  char buffer [80];
  std::time(&rawtime);
  timeinfo = std::localtime(&rawtime);
  std::strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
  return std::string(buffer);

}


std::string gstreamer_pipeline (int camera_id, int capture_width, int capture_height, int display_width, int display_height, int framerate, double brightness, int flip_method) {
    return "nvarguscamerasrc " + (camera_id < 0 ? "" : "sensor_id=" + std::to_string(camera_id) + " ")  + "! video/x-raw(memory:NVMM), width=(int)" + std::to_string(capture_width) + ", height=(int)" +
           std::to_string(capture_height) + ", framerate=(fraction)" + std::to_string(framerate) +
           "/1 ! nvvidconv flip-method=" + std::to_string(flip_method) + " ! video/x-raw, width=(int)" + std::to_string(display_width) + ", height=(int)" +
           std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! videobalance contrast=1.5 brightness=" + std::to_string(brightness) + " saturation=2.0 ! appsink";
      //     std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}

using time_point = std::chrono::system_clock::time_point;
std::string serializeTimePoint( const time_point& time, const std::string& format)
{
    std::time_t tt = std::chrono::system_clock::to_time_t(time);
    std::tm tm = *std::gmtime(&tt); //GMT (UTC)
    //std::tm tm = *std::localtime(&tt); //Locale time-zone, usually UTC by default.
    std::stringstream ss;
    ss << std::put_time( &tm, format.c_str() );
    return ss.str();
}

void CaptureVideo(
                 
                  const std::string& output_dir = "output") {
  // Open video
  cv::VideoCapture capture;
  std::string video_out_name = "output.mp4";
  bool is_file = false;

  int capture_width = 640 ;
  int capture_height = 360 ;
  int display_width = 640 ;
  int display_height = 360 ;
  int framerate = FLAGS_frame_rate ;
  int flip_method = 0 ;

  std::string pipeline = gstreamer_pipeline(FLAGS_camera_id, capture_width,
  capture_height,
  display_width,
  display_height,
  framerate,
  FLAGS_brightness,
  flip_method);


  printf("Try to open camera %s...\n", pipeline.c_str());


  capture.open(pipeline, cv::CAP_GSTREAMER);

  if (!capture.isOpened()) {
    printf("can not open camera!\n");
    return;
  }

  printf("Succeed openning camera, begin to capture video!\n");

  // Get Video info : resolution, fps, frame count
  int video_width = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
  int video_height = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
  int video_fps = static_cast<int>(capture.get(cv::CAP_PROP_FPS));
  //int video_frame_count =
  //    static_cast<int>(capture.get(cv::CAP_PROP_FRAME_COUNT));
  //printf("fps: %d, frame_count: %d\n", video_fps, video_frame_count);
  printf("Camera video info, width: %d, height: %d, fps: %d,\n", video_width, video_height, video_fps);
  // Create VideoWriter for output
  cv::VideoWriter video_out;  
  std::string video_out_path(output_dir);
  if (output_dir.rfind(OS_PATH_SEP) != output_dir.size() - 1) {
    video_out_path += OS_PATH_SEP;
  }

  video_out_path += GenerateTimeStr() + "_" + video_out_name;

  int fcc = cv::VideoWriter::fourcc('X', '2', '6', '4');
  video_out.open(video_out_path.c_str(),
                 fcc,
                 video_fps,
                 cv::Size(video_width, video_height),
                 true);
  if (!video_out.isOpened()) {
    printf("create video writer failed!\n");
    return;
  }


  // Capture all frames and do inference
  cv::Mat frame;
  int frame_id = 1;
  cv::namedWindow("Camera", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);//CV_WINDOW_NORMAL就是0
  cv::resizeWindow("Camera",cv::Size(640,360));
  cv::moveWindow("Camera",320,180);
  //cv::setWindowProperty("Action Recognition Demo", cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);

  float text_scale = std::max(1, int(video_width / 1600.));
  float text_thickness = 2.;
  float line_thickness = std::max(1, int(video_width / 500.));
  cv::Point origin;
  origin.x = 0;
  origin.y = int(15 * text_scale);

  bool stop_flag = false;

  auto start_time = std::chrono::steady_clock::now();
  auto current_time = std::chrono::system_clock::now();


  while (true) { 
    cv::Mat frame;
    capture.read(frame);

    if (frame.empty()) {
      break;
    }


    if ((frame_id % framerate) == 0) {
      current_time = std::chrono::system_clock::now();
    }

    auto time_str =  serializeTimePoint(current_time, "%H:%M:%S");
    cv::putText(frame,
                time_str,
                origin,
                cv::FONT_HERSHEY_PLAIN,
                text_scale,
                (0, 0, 255),
                2);
    video_out.write(frame);
    
    cv::imshow("Camera", frame);
    if (cv::waitKey(1) == char('q')) {
      printf("Key Q Pressed, Exitting!\n");
      break;
    }  

    if ((frame_id % 100) == 0) {
      auto end_time = std::chrono::steady_clock::now();

      const double wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                  start_time)
            .count();

      LOG(INFO) << "Process " << frame_id << " frames, frame rate: " << std::setprecision(4) << 100 / wall_clock_seconds << "fps!";
      start_time = end_time;
      
    }
    frame_id++;
 
  }

  capture.release();
  video_out.release();

  printf("Captured output saved as %s\n", video_out_path.c_str());
}

int main(int argc, char** argv) {
  // Parsing command-line
  google::ParseCommandLineFlags(&argc, &argv, true);
   

  if (!PathExists(FLAGS_output_dir)) {
    MkDirs(FLAGS_output_dir);
  }

  CaptureVideo(FLAGS_output_dir);

  return 0;
}
