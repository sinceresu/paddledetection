#pragma once

#include <string>
#include <memory>
#include <vector>
#include <deque>

#include <opencv2/opencv.hpp>


typedef struct AVCodec AVCodec;
typedef struct AVCodecContext AVCodecContext;
typedef struct AVFrame AVFrame;
typedef struct AVFormatContext AVFormatContext;
typedef struct AVStream AVStream;
typedef struct AVPacket AVPacket;
typedef struct SwsContext SwsContext;
typedef struct SwsContext SwsContext;



class Streamer {
public:
  Streamer();
  ~Streamer();

  Streamer(const Streamer&) = delete;
  Streamer& operator=(const Streamer&) = delete;
  int Initialize(int image_width, int image_heights, const std::string & streaming_url, int frame_rate, uint64_t start_time_ms);
  void Release();
  void SetEncodeParam(int key_frame_interval, int constant_rate_factor);

  // int Send(const std::vector<cv::Mat>& image_yuv, const Eigen::Affine3d& pose, int64_t timestamp_ms);
  int SendImage(const cv::Mat& image_bgr, int64_t timestamp_ms);

private:
  // std::string GenerateSEIHead(const Eigen::Affine3d& pose);
  const AVCodec *codec;
  AVCodecContext      *enc_ctx_;

  SwsContext *img_convert_ctx_;

  
  AVFormatContext *ofmt_ctx_;
  AVStream *video_st_;
  std::string codec_name_;
  AVFrame * frame_;
  AVFrame * bgr_frame_;

  AVPacket*  pkt_;
  std::string stream_url_;
  int key_frame_interval_;
  int constant_rate_factor_;

};

